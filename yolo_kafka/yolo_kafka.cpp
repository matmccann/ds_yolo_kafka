#include <gst/gst.h>
#include <glib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <cuda_runtime_api.h>
#include "src_bin.h"
#include <cmath>
#include <string>
#include <list>
#include <vector>
#include <unistd.h>

#include <stdio.h>
#include "nvds_msgapi.h"
/* msgconv libraries*/
#include "mist_msgconv.h"

using namespace std;
int tiler_frame_number = 0;

const gchar *get_link_error(int error_code);
void split_streams(GstElement *element, GstPad *pad, TsDemuxLinks *sinks);


/* manage bus callbacks from the pipeline */
static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data) {
    GMainLoop *loop = (GMainLoop *) data;

    switch (GST_MESSAGE_TYPE(msg)) {

        case GST_MESSAGE_EOS:
            g_print("End of stream\n");
            g_main_loop_quit(loop);
            break;

        case GST_MESSAGE_ERROR: {
            gchar *debug;
            GError *error;

            gst_message_parse_error(msg, &error, &debug);
            g_free(debug);

            g_printerr("Error: %s\n", error->message);
            g_error_free(error);

            g_main_loop_quit(loop);
            break;
        }
        default:
            break;
    }

    return TRUE;
}

int main(int argc, char *argv[]) {
    /**
     * Runs a multi-src gstreamer pipeline to NN ensemble with two sinks.
     * (1) kafka payload
     * (2) EGL display
     *
     */

    /* Set up gstreamer loops */
    GMainLoop *loop = NULL;
    GstBus *bus = NULL;
    guint bus_watch_id;

    /* get Cuda properties from hardware */
    int current_device = -1;
    cudaGetDevice(&current_device);
    struct cudaDeviceProp prop;
    cudaGetDeviceProperties(&prop, current_device);

    /* Check input arguments */
    if (argc != 3) {
        // g_printerr("Usage: %s <uri1> [uri2] ... [uriN] \n", argv[0]);
        g_printerr("Usage: %s </path/to/file.ts> <num_streams> <0,1: include klv stream> \n", argv[0]);
        g_printerr("Usage: %s 2 0 \n", argv[0]);
        exit(-1);
    }

    const gchar *pgie_yolo = "/home/nvidia/code/seamist/yolo_kafka/ds_configs/pgie.txt";

    /* Initialization */
    int index = 0;
    int num_sources = atoi(argv[1]);
    bool klv_flag = strcmp(argv[2], "0"); // true is argv[2] is 1
    std::vector < std::string > video_streams = {
            "/jetson_media/videos/seamist/HalifaxTop.ts",
            "/jetson_media/videos/seamist/HalifaxBottom.ts",
            "/jetson_media/videos/seamist/HalifaxTop.ts",
            "/jetson_media/videos/seamist/HalifaxBottom.ts"
    };
    /* Set up gst basics */
    gst_init(&argc, &argv);
    loop = g_main_loop_new(NULL, FALSE);

    /* Instantiate pipeline */
    Inference pipelineClass;
    pipelineClass.setup_kafka();
    pipelineClass.setup_pipeline(num_sources, index, pgie_yolo);

    InferencePipeline pipeline = pipelineClass.pipeline_container[0];

    /* Create ingestion pipeline for each video source */
    SourceBin srcbin;
    while (index < num_sources) {
        srcbin.create_src_bin(&pipeline, video_streams[index].c_str(), index, klv_flag, "filesrc");
        g_print("--(SrcBin %d) video source added: %s\n", index, video_streams[index].c_str());
        index++;
    }

    /* Add dynamic links after all source bins have been created TODO: gst_stream_new(index_parse); */
    for (int index_parse = 0; index_parse < num_sources; index_parse++) {
        g_print("--(src name %d)-- parsing tsdemux_links ... \n", index_parse);
        TsDemuxLinks links = srcbin.bin_container[index_parse].tsdemux_links;
        srcbin.set_tsdemux_links(index_parse);
    };

    /* Link elements to create directional data flow */
    gst_element_link(pipeline.streammux, pipeline.pgie);
    if (pipeline.num_sources != 1) {
        gst_element_link(pipeline.pgie, pipeline.tiler);
        gst_element_link(pipeline.tiler, pipeline.nvvidconv);
    } else {
        gst_element_link(pipeline.pgie, pipeline.nvvidconv);
    }
    gst_element_link_many(pipeline.nvvidconv, pipeline.nvosd, pipeline.transform, pipeline.sink, NULL);

    /* Set bus callback to manage pipeline state */
    bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline.pipeline));
    bus_watch_id = gst_bus_add_watch(bus, bus_call, loop);
    gst_object_unref(bus);

    /* Set the pipeline to "PLAYING" state*/
    g_print("-- PIPELINE STATE CHANGE: PLAYING -- \n");
    gst_element_set_state(pipeline.pipeline, GST_STATE_PLAYING);

    /* Runs loop until completion */
    g_main_loop_run(loop);

    /* Out of the main loop, so clean up references */
    g_print("-- Returned, stopping playback -- \n");
    gst_element_set_state(pipeline.pipeline, GST_STATE_NULL);
    g_print("-- Deleting pipeline -- \n");
    gst_object_unref(GST_OBJECT(pipeline.pipeline));
    g_source_remove(bus_watch_id);
    g_main_loop_unref(loop);
    g_print("-- SUCCESS: exit. -- \n");
    exit(0);
}