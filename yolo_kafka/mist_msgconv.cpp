#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <gst/gst.h>
#include <json-glib/json-glib.h>
#include <glib.h>
#include "nvds_msgapi.h"
#include <array>
#include <unordered_map>
#include <uuid.h>
#include <time.h>
#include <stdint.h>
#include <inttypes.h>
#include "mist_msgconv.h"

#ifdef __cplusplus
extern "C"
{
#endif

using namespace std;

struct SensorObject {
    char id[20];
    char type[20];
    char description[20];
    gdouble location[3];
    gdouble coordinate[3];
};

struct DetectionObject {
    char detector[20];
    char classifier[20];
    gdouble det_conf;
    gdouble cla_conf;
};

struct klvObject {
    char timestamp[20];
    int64_t ts_epoch;
    int klv_frame_number;
    gdouble acft_hdg;
    gdouble sensor_lat;
    gdouble sensor_lon;
    gdouble sensor_alt;
    gdouble hfov;
    gdouble vfov;
    gdouble sensor_az;
    gdouble slant_range;
    gdouble target_width;
    gdouble center_lat;
    gdouble center_lon;
    gdouble center_elev;
    int sensor_id;
};

struct PayloadMist {
    unordered_map<int, klvObject> klvObj;
    unordered_map<int, SensorObject> sensorObj;
    unordered_map<int, DetectionObject> detectionObj;
};

int64_t generate_ts_epoch(void) {

    struct timespec tms;
    int64_t micros;
    /* The C11 way */
    if (!timespec_get(&tms, TIME_UTC)) {
        g_printerr("[generate_timestamp] failed to create timestamp ... \n");
        micros = 0;
        return micros;
    }
    /* seconds, multiplied with 1 million */
    micros = tms.tv_sec * 1000000;
    /* Add full microseconds */
    micros += tms.tv_nsec / 1000;
    /* round up if necessary */
    if (tms.tv_nsec % 1000 >= 500) {
        ++micros;
    }
    return micros;
}

JsonObject *generate_ds_object(DsMeta metadata) {
    JsonObject *dsPayloadObj;
    dsPayloadObj = json_object_new();
    // detection information
    json_object_set_int_member(dsPayloadObj, "unique_component_id", metadata.unique_component_id);
    json_object_set_int_member(dsPayloadObj, "class_id", metadata.class_id);
    json_object_set_int_member(dsPayloadObj, "object_id", metadata.object_id);
    json_object_set_double_member(dsPayloadObj, "detector_conf", metadata.confidence);
    json_object_set_double_member(dsPayloadObj, "tracker_conf", metadata.tracker_confidence);
    // bounding box points
    json_object_set_double_member(dsPayloadObj, "xmin", metadata.bbox.xmin);
    json_object_set_double_member(dsPayloadObj, "xmax", metadata.bbox.xmax);
    json_object_set_double_member(dsPayloadObj, "ymin", metadata.bbox.ymin);
    json_object_set_double_member(dsPayloadObj, "ymax", metadata.bbox.ymax);
    json_object_set_string_member(dsPayloadObj, "detector_label", metadata.obj_label);
    return dsPayloadObj;
}

JsonObject *generate_sensor_object(guint source_id) {
    SensorObject *meta = (SensorObject *) g_malloc0(sizeof(SensorObject));


//    strcpy(meta->id, "id");
    strcpy(meta->type, "type");
    strcpy(meta->description, "description");
    meta->coordinate[0] = 1;
    meta->coordinate[1] = 1;
    meta->coordinate[2] = 1;
    meta->location[0] = 1;
    meta->location[1] = 1;
    meta->location[2] = 1;

//    printf("sensorObj (id): %s \n", meta->id);
//    printf("sensorObj (type): %s \n", meta->type);
//    printf("sensorObj (description): %s \n", meta->description);
//    printf("sensorObj (location): [%f, %f, %f] \n", meta->location[0], meta->location[1], meta->location[2]);
//    printf("sensorObj (coordinate): [%f, %f, %f] \n", meta->coordinate[0], meta->coordinate[1], meta->coordinate[2]);

    /* sensor object
     * "sensor": {
         "id": "id_string",
         "type": "name_type",
         "location": {
           "lat": 45.99,
           "lon": 35.54,
           "alt": 79.03
         },
         "coordinate": {
           "x": 5.2,
           "y": 10.1,
           "z": 11.2
         },
         "description": "add_description"
       }
     */

    JsonObject *sensorObj;
    JsonObject *jobject;
    // sensor object
    sensorObj = json_object_new();
    json_object_set_int_member(sensorObj, "id", source_id);
    json_object_set_string_member(sensorObj, "type", meta->type);
    json_object_set_string_member(sensorObj, "description", meta->description);

    // location sub object
    jobject = json_object_new();
    json_object_set_double_member(jobject, "lat", meta->location[0]);
    json_object_set_double_member(jobject, "lon", meta->location[1]);
    json_object_set_double_member(jobject, "alt", meta->location[2]);
    json_object_set_object_member(sensorObj, "location", jobject);

    // coordinate sub object
    jobject = json_object_new();
    json_object_set_double_member(jobject, "x", meta->coordinate[0]);
    json_object_set_double_member(jobject, "y", meta->coordinate[1]);
    json_object_set_double_member(jobject, "z", meta->coordinate[2]);
    json_object_set_object_member(sensorObj, "coordinate", jobject);
    return sensorObj;
}

JsonObject *generate_klv_object(void) {
    JsonObject *klvPayloadObj;

    klvObject *klvPayload = (klvObject *) g_malloc0(sizeof(klvObject));
    strcpy(klvPayload->timestamp, "2018-04-11T04:59:59.828");
    klvPayload->ts_epoch = generate_ts_epoch();
    klvPayload->klv_frame_number = 1;
    klvPayload->acft_hdg = 1;
    klvPayload->sensor_lat = 1;
    klvPayload->sensor_lon = 1;
    klvPayload->sensor_alt = 1;
    klvPayload->hfov = 1;
    klvPayload->vfov = 1;
    klvPayload->sensor_az = 1;
    klvPayload->slant_range = 1;
    klvPayload->target_width = 1;
    klvPayload->center_lat = 1;
    klvPayload->center_lon = 1;
    klvPayload->center_elev = 1;
    klvPayload->sensor_id = 1;

    klvPayloadObj = json_object_new();
    json_object_set_string_member(klvPayloadObj, "timestamp", klvPayload->timestamp);
    json_object_set_int_member(klvPayloadObj, "ts_epoch", klvPayload->ts_epoch);
    json_object_set_int_member(klvPayloadObj, "klv_frame_number", klvPayload->klv_frame_number);
    json_object_set_double_member(klvPayloadObj, "acft_hdg", klvPayload->acft_hdg);
    json_object_set_double_member(klvPayloadObj, "sensor_lat", klvPayload->sensor_lat);
    json_object_set_double_member(klvPayloadObj, "sensor_lon", klvPayload->sensor_lon);
    json_object_set_double_member(klvPayloadObj, "sensor_alt", klvPayload->sensor_alt);
    json_object_set_double_member(klvPayloadObj, "hfov", klvPayload->hfov);
    json_object_set_double_member(klvPayloadObj, "vfov", klvPayload->vfov);
    json_object_set_double_member(klvPayloadObj, "sensor_az", klvPayload->sensor_az);
    json_object_set_double_member(klvPayloadObj, "slant_range", klvPayload->slant_range);
    json_object_set_double_member(klvPayloadObj, "target_width", klvPayload->target_width);
    json_object_set_double_member(klvPayloadObj, "center_lat", klvPayload->center_lat);
    json_object_set_double_member(klvPayloadObj, "center_lon", klvPayload->center_lon);
    json_object_set_double_member(klvPayloadObj, "center_elev", klvPayload->center_elev);
    json_object_set_int_member(klvPayloadObj, "sensor_id", klvPayload->sensor_id);
    return klvPayloadObj;
}

gchar *generate_schema_message(DsMeta metadata, int frame) {

    JsonNode *rootNode;
    JsonObject *rootObj;
    JsonObject *sensorObj;
    JsonObject *klvObj;
    JsonObject *dsObj;
    gchar *message;
    uuid_t msgId;
    gchar msgIdStr[37];

    uuid_generate_random(msgId);
    uuid_unparse_lower(msgId, msgIdStr);

    // generate ts epoch timestamp in microseconds
    int64_t ts = generate_ts_epoch();
    // sensor object
    sensorObj = generate_sensor_object(metadata.source_id);
    klvObj = generate_klv_object();
    dsObj = generate_ds_object(metadata);

    // root object
    rootObj = json_object_new();
    json_object_set_string_member(rootObj, "uuid", msgIdStr);
    json_object_set_int_member(rootObj, "ts", metadata.ntp_timestamp);
    json_object_set_int_member(rootObj, "ts_epoch", ts);
    json_object_set_object_member(rootObj, "neural_network", dsObj);
    json_object_set_object_member(rootObj, "sensor", sensorObj);
    json_object_set_object_member(rootObj, "klv", klvObj);

    rootNode = json_node_new(JSON_NODE_OBJECT);
    json_node_set_object(rootNode, rootObj);

    message = json_to_string(rootNode, true);
    json_node_free(rootNode);
    json_object_unref(rootObj);
    return message;
}

#ifdef __cplusplus
}
#endif
