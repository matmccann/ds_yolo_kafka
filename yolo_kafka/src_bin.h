#ifndef MIST_SRC_BIN_H
#define MIST_SRC_BIN_H

#include <string>
#include <gst/gst.h>
#include <glib.h>
#include <stdbool.h>
#include <stdlib.h>
#include <vector>
//#include "gstnvdsmeta.h"
#include "nvds_msgapi.h"
#include "mist_msgconv.h"
#ifdef __cplusplus
extern "C"
{
#endif

/* Kafka library path and configs */
#define KAFKA_PROTO_PATH "/opt/nvidia/deepstream/deepstream/lib/mist_kafka_proto.so"
#define KAFKA_CFG_FILE "ds_configs/cfg_kafka.txt"
#define KAFKA_CONNECT_STR "172.22.0.3;9092"
#define KAFKA_TOPIC "ir"
#define KAFKA_GROUP "seamist"
#define MAX_LEN 256
/* streammux and display configs */
#define MUXER_BATCH_TIMEOUT_USEC 40000
#define MUXER_OUTPUT_WIDTH 1920
#define MUXER_OUTPUT_HEIGHT 1080
#define TILED_OUTPUT_WIDTH 1280
#define TILED_OUTPUT_HEIGHT 720



using namespace std;

typedef struct _TsDemuxLinks {
    GstElement *video;
    GstElement *klv;
} TsDemuxLinks;

typedef struct _SrcBin {
    GstElement *source;
    GstElement *tsparse;
    GstElement *tsdemux;
    GstElement *codec_parser;
    GstElement *hardware_decoder;
    GstElement *klv_parser;
    GstElement *capsfilter;
    GstElement *klv_sink;
    TsDemuxLinks tsdemux_links;
    char *video_file;
    bool klv_flag;
} SrcBin;

typedef struct _InferencePipeline {
    GstElement *pipeline;
    GstElement *streammux;
    GstElement *pgie;
    GstElement *tracker;
    GstElement *tiler; // NVDS metadata looses reference to NvDsFrameMeta `source_id` after this element!
    GstElement *nvvidconv;
    GstElement *nvosd;
    GstElement *transform;
    GstElement *sink;
    int num_sources;
} InferencePipeline;

typedef struct _kafkaProducer {
    NvDsMsgApiHandle conn_handle;
    NvDsMsgApiHandle(*msgapi_connect_ptr)(char * connection_str, nvds_msgapi_connect_cb_t connect_cb, char * config_path);
    NvDsMsgApiErrorType(*msgapi_send_async_ptr)(NvDsMsgApiHandle h_ptr, char * topic, const uint8_t *payload, size_t nbuf, nvds_msgapi_send_cb_t send_callback, void * user_ptr);
    NvDsMsgApiErrorType(*msgapi_disconnect_ptr)(NvDsMsgApiHandle h_ptr);
    bool connection;
    std::string topic;
    gint payload_count;
} kafkaProducer;

void kafka_send_cb(void *user_ptr, NvDsMsgApiErrorType completion_flag);

static GstPadProbeReturn osd_sink_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info, gpointer u_data);

/**
* This function generates a human readable error code when pad linking fails.
*
* @param[in] error_code converts error code (GST_PAD_LINK_FAILED) from gst_pad_link().
*/
const gchar *get_link_error(int error_code);

/**
 * This function splits one mpegts into its streams. Links invoked tsDemux src pad (video/private)
 * to its desired sink element (i.e video_parser, klv_parser).
 *
 * @param[in] element is the element that has a new src pad to link to a sink pad.
 * @param[in] pad is the newly created pad from `element` that is ready to link.
 * @param[in] sinks uses a struct to link video and klv to its appropriate sinks when they exist.
 */
void split_streams(GstElement *element, GstPad *pad, TsDemuxLinks *bin);


class SourceBin {
public:
    std::vector<SrcBin> bin_container;

    /**
     * This function creates a MPEG-TS ingestion bin (`filesrc`) that may be linked to nv-streammux
     *
     * @param[in] bin the bin to which all elements are added.
     * @param[in] index unsigned into that is used to name the source.
     * @param[in] video_file is a path to the video.ts file that it will ingest.
     */
    void create_src_bin(InferencePipeline *pipeline, const char *video_file, int index, bool klv_flag, std::string element_name);
    void set_tsdemux_links(int index);
};


class Inference {

public:
    // gstreamer elements and pipeline
    std::vector<InferencePipeline> pipeline_container;
    // kafka configurations
    std::vector<kafkaProducer> kafka_producer;

    void setup_kafka();
    void setup_pipeline(int num_sources, int index, const gchar * pgie_yolo);
    static GstPadProbeReturn kafka_src_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info, gpointer probe_name);
};


#ifdef __cplusplus
}
#endif
#endif //MIST_SRC_BIN_H
