/*
 * src_bin.cpp
 *
 */

#include <string.h>
#include <gst/gst.h>
#include <glib.h>
#include <string>
#include <stdbool.h>
#include <stdlib.h>
#include <cuda_runtime_api.h>
#include <cmath>
#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <unistd.h>

#include "src_bin.h"
#include "mist_meta.h"
#include "gstnvdsmeta.h"
#include "nvds_msgapi.h"
#include "mist_msgconv.h"
#include <functional>

using namespace std;

/* Probe callback variables */
int streammux_frame_number = 0;
int nvinfer_frame_number = 0;
int nvvidconv_frame_number = 0;
int rcvd_frame_number = 0;
int generic_frame_number = 0;
int kafka_frame_number = 0;

/* kafka adaptor */
int g_cb_count = 0;

gchar *generate_schema_message(DsMeta metadata, int frame);
void msgapi_connect_cb(NvDsMsgApiHandle *h_ptr, NvDsMsgApiEventType ds_evt) {}

/* CB for kafka producer */
void kafka_send_cb(void *user_ptr, NvDsMsgApiErrorType completion_flag) {
    // printf("async send complete (from kafka_send_cb)\n");
    if (completion_flag == NVDS_MSGAPI_OK)
        g_print("%s successfully \n", (char *) user_ptr);
    else
        g_print("%s with failure\n", (char *) user_ptr);
    g_cb_count++;
}


/* CB for streammux src pad probe */
static GstPadProbeReturn streammux_src_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info, gpointer u_data) {

    gchar *src_pad_name = gst_pad_get_name(pad);

    GstBuffer *buf = (GstBuffer *) info->data;
    guint num_rects = 0;
    NvDsObjectMeta *obj_meta = NULL;
    NvDsMetaList *l_frame = NULL;
    NvDsMetaList *l_obj = NULL;
    NvDsDisplayMeta *display_meta = NULL;

    g_print("\n********  streammux_src_pad_buffer_probe (osd::%d, streammux::%d) (%s) ************* \n",
            rcvd_frame_number, streammux_frame_number, src_pad_name);

    /* deconstruct the NvDsBatchMeta structure in the gstreamer buffer */
    NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta(buf);
    guint num_frames_in_batch = batch_meta->num_frames_in_batch;
    g_print(">>>> NvDsBatchMeta \n");
    g_print("     num_frames_in_batch (%u) \n", num_frames_in_batch);

    for (l_frame = batch_meta->frame_meta_list; l_frame != NULL; l_frame = l_frame->next) {
        /* deconstruct the NvDsFrameMeta structure in the gstreamer buffer */
        NvDsFrameMeta *frame_meta = (NvDsFrameMeta * )(l_frame->data);
        guint source_id = frame_meta->source_id;
        int offset = 0;
        gint frame_number = frame_meta->frame_num;
        guint64 buf_pts = frame_meta->buf_pts;
        guint64 ntp_timestamp = frame_meta->ntp_timestamp;

        gint num_surfaces_per_frame = frame_meta->num_surfaces_per_frame;
        guint surface_index = frame_meta->surface_index;
        guint num_obj_meta = frame_meta->num_obj_meta;
        gboolean bInferDone = frame_meta->bInferDone;
//        gint64 *misc_frame_info = frame_meta->misc_frame_info;

        g_print(">>>> NvDsFrameMeta \n");
        g_print("     frame_number (%d), buf_pts (%lu), ntp_ts (%lu), source_id (%u) \n", frame_number, buf_pts,
                ntp_timestamp, source_id);
        g_print("     num_surfaces_per_frame (%d), surface_index (%u), num_obj_meta (%u) \n", num_surfaces_per_frame,
                surface_index, num_obj_meta);
        g_print("     bInferDone (%d), misc_frame_info () \n", bInferDone);
//        nvds_add_frame_meta_to_batch(batch_meta, frame_meta);
    }

    streammux_frame_number++;
    return GST_PAD_PROBE_OK;
}


/* CB for nvinfer sink pad probe */
static GstPadProbeReturn nvinfer_sink_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info, gpointer u_data) {

    gchar *src_pad_name = gst_pad_get_name(pad);

    GstBuffer *buf = (GstBuffer *) info->data;
    guint num_rects = 0;
    NvDsObjectMeta *obj_meta = NULL;
    NvDsMetaList *l_frame = NULL;
    NvDsMetaList *l_obj = NULL;
    NvDsDisplayMeta *display_meta = NULL;

    g_print("\n********  nvinfer_sink_pad_buffer_probe (osd::%d, nvinfer::%d) (%s) ************* \n",
            rcvd_frame_number, nvinfer_frame_number, src_pad_name);

    /* deconstruct the NvDsBatchMeta structure in the gstreamer buffer */
    NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta(buf);
    guint num_frames_in_batch = batch_meta->num_frames_in_batch;
    g_print(">>>> NvDsBatchMeta \n");
    g_print("     num_frames_in_batch (%u) \n", num_frames_in_batch);

    for (l_frame = batch_meta->frame_meta_list; l_frame != NULL; l_frame = l_frame->next) {
        /* deconstruct the NvDsFrameMeta structure in the gstreamer buffer */
        NvDsFrameMeta *frame_meta = (NvDsFrameMeta * )(l_frame->data);
        guint source_id = frame_meta->source_id;
        int offset = 0;
        gint frame_number = frame_meta->frame_num;
        guint64 buf_pts = frame_meta->buf_pts;
        guint64 ntp_timestamp = frame_meta->ntp_timestamp;

        gint num_surfaces_per_frame = frame_meta->num_surfaces_per_frame;
        guint surface_index = frame_meta->surface_index;
        guint num_obj_meta = frame_meta->num_obj_meta;
        gboolean bInferDone = frame_meta->bInferDone;
//        gint64 *misc_frame_info = frame_meta->misc_frame_info;

        g_print(">>>> NvDsFrameMeta \n");
        g_print("     frame_number (%d), buf_pts (%lu), ntp_ts (%lu), source_id (%u) \n", frame_number, buf_pts,
                ntp_timestamp, source_id);
        g_print("     num_surfaces_per_frame (%d), surface_index (%u), num_obj_meta (%u) \n", num_surfaces_per_frame,
                surface_index, num_obj_meta);
        g_print("     bInferDone (%d), misc_frame_info () \n", bInferDone);
//        nvds_add_frame_meta_to_batch(batch_meta, frame_meta);
    }

    nvinfer_frame_number++;
    return GST_PAD_PROBE_OK;
}


/* CB for generic probe pad */
static GstPadProbeReturn generic_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info, gpointer u_data) {

    GstBuffer *buf = (GstBuffer *) info->data;
    /* deconstruct the NvDsBatchMeta structure in the gstreamer buffer */
    NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta (buf);
    guint num_frames_in_batch = batch_meta->num_frames_in_batch;
    g_print("\n********  probe_name (%s) (osd::%d, frame::%d) frames (%u) ************* \n",
            (gchar *) u_data, rcvd_frame_number, generic_frame_number, num_frames_in_batch);

    /* update class counter to track frames received */
    generic_frame_number++;
    return GST_PAD_PROBE_OK;
}


/* CB for osd sink pad probe */
static GstPadProbeReturn osd_sink_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info, gpointer u_data) {
    GstBuffer *buf = (GstBuffer *) info->data;
    guint num_rects = 0;
    NvDsObjectMeta *obj_meta = NULL;
    NvDsMetaList * l_frame = NULL;
    NvDsMetaList * l_obj = NULL;
    NvDsDisplayMeta *display_meta = NULL;
    g_print("\n********  osd_sink_pad_buffer_probe (%d) ************* \n", rcvd_frame_number);

    /* deconstruct the NvDsBatchMeta structure in the gstreamer buffer */
    NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta (buf);
    guint num_frames_in_batch = batch_meta->num_frames_in_batch;
    g_print(">>>> NvDsBatchMeta \n");
    g_print("     num_frames_in_batch (%u) \n", num_frames_in_batch);

    for (l_frame = batch_meta->frame_meta_list; l_frame != NULL; l_frame = l_frame->next) {
        /* deconstruct the NvDsFrameMeta structure in the gstreamer buffer */
        NvDsFrameMeta *frame_meta = (NvDsFrameMeta *) (l_frame->data);
        guint source_id = frame_meta->source_id;
        int offset = 0;
        gint frame_number = frame_meta->frame_num;
        guint64 buf_pts = frame_meta->buf_pts;
        guint64 ntp_timestamp = frame_meta->ntp_timestamp;

        gint num_surfaces_per_frame = frame_meta->num_surfaces_per_frame;
        guint surface_index = frame_meta->surface_index;
        guint num_obj_meta = frame_meta->num_obj_meta;
        gboolean bInferDone = frame_meta->bInferDone;
//        gint64 *misc_frame_info = frame_meta->misc_frame_info;

        g_print(">>>> NvDsFrameMeta \n");
        g_print("     frame_number (%d), buf_pts (%lu), ntp_ts (%lu), source_id (%u) \n", frame_number, buf_pts, ntp_timestamp, source_id);
        g_print("     num_surfaces_per_frame (%d), surface_index (%u), num_obj_meta (%u) \n", num_surfaces_per_frame, surface_index, num_obj_meta);
        g_print("     bInferDone (%d), misc_frame_info () \n", bInferDone);

        /* deconstruct the NvDsObjectMeta structure in the gstreamer buffer */
        int objects_detected = 0;
        for (l_obj = frame_meta->obj_meta_list; l_obj != NULL; l_obj = l_obj->next) {
            obj_meta = (NvDsObjectMeta *) (l_obj->data);

            /* standard object metadata fields */
            gint unique_component_id = obj_meta->unique_component_id;
            gint class_id = obj_meta->class_id;
            guint64 object_id = obj_meta->object_id;
//            NvDsComp_BboxInfo detector_bbox_info = obj_meta->detector_bbox_info;
//            NvDsComp_BboxInfo tracker_bbox_info = obj_meta->tracker_bbox_info;
            gfloat confidence = obj_meta->confidence;
            gfloat tracker_confidence = obj_meta->tracker_confidence;
//            NvOSD_RectParams rect_params = obj_meta->rect_params;
//            NvOSD_MaskParams mask_params = obj_meta->mask_params;
//            NvOSD_TextParams text_params = obj_meta->text_params;
            gchar *obj_label = obj_meta->obj_label;
            gint64 *misc_obj_info = obj_meta->misc_obj_info;
            g_print(">>>> NvDsObjectMeta (object # %d) \n", objects_detected);
            g_print("     unique_component_id (%d), class_id (%d), object_id (%lu) \n", unique_component_id, class_id, object_id);
            g_print("     detector_bbox_info (), tracker_bbox_info (), confidence (%f), tracker_confidence (%f) \n", confidence, tracker_confidence);
            g_print("     rect_params (), mask_params (), text_params () \n");
            g_print("     obj_label (%s), misc_obj_info () \n", obj_label);
//                    detector_bbox_info, tracker_bbox_info, confidence, tracker_confidence,
//                    rect_params, mask_params, text_params,
//                    obj_label, misc_obj_info
            objects_detected ++;
        }
    }

    /* update class counter to track frames received */
    rcvd_frame_number++;
    return GST_PAD_PROBE_OK;
}


/* Translates link_pad() error to a readable error */
const gchar *get_link_error(int error_code) {
    const gchar *ret_str = "ERROR: get_link_error() doesn't recognize error_code";
    switch (error_code) {
        case 0:
            ret_str = "GST_PAD_LINK_OK (0) – link succeeded";
            break;
        case -1:
            ret_str = "GST_PAD_LINK_WRONG_HIERARCHY (-1) – pads have no common grandparent";
            break;
        case -2:
            ret_str = "GST_PAD_LINK_WAS_LINKED (-2) – pad was already linked";
            break;
        case -3:
            ret_str = "GST_PAD_LINK_WRONG_DIRECTION (-3) – pads have wrong direction";
            break;
        case -4:
            ret_str = "GST_PAD_LINK_NOFORMAT (-4) – pads do not have common format";
            break;
        case -5:
            ret_str = "GST_PAD_LINK_NOSCHED (-5) – pads cannot cooperate in scheduling";
            break;
        case -6:
            ret_str = "GST_PAD_LINK_REFUSED (-6) – refused for some reason";
            break;
    }
    return ret_str;
}


/*
 * Splits one mpegts into its streams. Links invoked tsDemux src pad (video/private)
 * to its desired sink element (i.e video_parser, klv_parser).
*/
void split_streams(GstElement *element, GstPad *pad, TsDemuxLinks *bin) {
    /* tsDemux callback function: handles invocation of its "Element Signal" "pad-added". */
    GstPadLinkReturn ret;
    GstElement *decoder;
    gchar *src_name = gst_element_get_name(element);
    gchar *src_pad_name = gst_pad_get_name(pad);

    g_print("--(link %s )-- src_pad (%s) \n", src_name, src_pad_name);

    /* Check if this link is called for video or klv */
    gchar *link_klv = strstr(src_pad_name, "private");
    gchar *link_video = strstr(src_pad_name, "video");
    g_print("--(link %s)-- <%s> parsed new pad for names: <video::%s>, <private::%s> \n",
            src_name, src_name, link_video, link_klv);

    if (!link_video && !link_klv) {
        g_printerr("--(link %s)-- cannot detect `video` or `private` stream on src pad \n", src_name);
        return;
    }

    if (link_video) {
        g_print("--(link %s)-- detected video pad \n", src_name);
        decoder = (GstElement *) bin->video;
        g_print("--(link %s)-- rcvd video element for linking \n", src_name);
    } else if (link_klv) {
        g_print("--(link %s)-- detected klv pad \n", src_name);
        if (bin->klv == NULL) {
            g_print("--(link %s)-- ignoring klv pad \n", src_name);
            return;
        }
        decoder = (GstElement *) bin->klv;
        g_print("--(link %s)-- rcvd klv element for linking \n", src_name);
    } else {
        g_printerr("--(link %s)-- ERROR: detected unknown stream! \n", src_name);
        g_printerr("--(link %s)-- ERROR: Incorrect configs ... FAIL SILENTLY! \n", src_name);
        return;
    }

    if (!decoder) {
        g_printerr(
                "--(link %s)-- ERROR: (video: %d, klv: %d) decoder pad not created ... attempting to print pad name. \n",
                src_name, bool(link_video), bool(link_klv));
        g_printerr("--(link %s)-- bin->video: name(%s) \n", src_name, gst_element_get_name(bin->video));
        return;
    }
    g_print("--(link %s)-- Attempting get decoder name ... \n", src_name);
    gchar *sink_name = gst_element_get_name(decoder);
    g_print("--(link %s)-- Attempting get static pad (decoder::%s) ... \n", src_name, sink_name);
    GstPad *sinkpad = gst_element_get_static_pad(decoder, "sink");
    g_print("--(link %s)-- Attempting get static pad name (sinkpad) ... \n", src_name);
    gchar *sink_pad_name = gst_pad_get_name(sinkpad);
    g_print("--(link %s)-- src (%s::%s); sink (%s::%s)  \n", src_name, src_name, src_pad_name, sink_name, sink_pad_name);

    /* If our converter is already linked, we have nothing to do here */
    if (gst_pad_is_linked(sinkpad)) {
        g_print("--(link %s)-- %s::%s is already linked \n", src_name, sink_name, sink_pad_name);
        gst_object_unref(sinkpad);
        return;
    }

    /* Link the stream */
    ret = gst_pad_link(pad, sinkpad);
    if (GST_PAD_LINK_FAILED(ret)) {
        const gchar *ret_str = get_link_error(ret);
        g_printerr("--(link %s)-- link response: %s \n", src_name, ret_str);
        return;
    }
    gst_object_unref(sinkpad);

    g_print("--(link %s)-- SUCCESS \n", src_name);
    return;
}



void SourceBin::create_src_bin (InferencePipeline *pipeline, const char *video_file, int index, bool klv_flag, std::string source_element_name) {
    /**
     * Create a source bin that may link to a main pipeline.
     * MPEGTS file sources: filesrc
     *
     */
    g_print("--(SourceBin::create_src_bin) Source bin setup for `%s` started. \n", source_element_name.c_str());
    gboolean klv_dump = TRUE;
    TsDemuxLinks tsdemux_links;
    /* Create source elements */
    SrcBin bin = {
            (GstElement *) gst_element_factory_make(source_element_name.c_str(), g_strdup_printf("source_%i", index)),
            (GstElement *) gst_element_factory_make("tsparse", g_strdup_printf("tsparse_%i", index)),
            (GstElement *) gst_element_factory_make("tsdemux", g_strdup_printf("tsdemux_%i", index)),
            (GstElement *) gst_element_factory_make("h264parse", g_strdup_printf("codec_parser_%i", index)),
            (GstElement *) gst_element_factory_make("nvv4l2decoder", g_strdup_printf("hardware_decoder_%i", index)),
            (GstElement *) gst_element_factory_make("queue", g_strdup_printf("klv_parser_%i", index)),
            (GstElement *) gst_element_factory_make("capsfilter", g_strdup_printf("capsfilter_%i", index)),
            (GstElement *) gst_element_factory_make("fakesink", g_strdup_printf("klv_sink_%i", index)),
            tsdemux_links,
            (char *) video_file,
            (bool) klv_flag,
    };

    /* Validate elements were created */
    if (!bin.source || !bin.tsparse || !bin.tsdemux) {
        g_printerr("--(SourceBin::create_src_bin: fail)-- not all ingestion stream elements were created. Exiting.\n");
        throw NULL;
    }
    if (!bin.codec_parser || !bin.hardware_decoder) {
        g_printerr("--(SourceBin::create_src_bin: fail)-- not all video stream elements were created. Exiting.\n");
        throw NULL;
    }

    /* Set object properties */
    g_object_set(G_OBJECT(bin.source), "location", bin.video_file, NULL);
    g_object_set(G_OBJECT(bin.tsdemux), "parse-private-sections", bin.klv_flag, NULL);
    g_object_set(G_OBJECT(bin.hardware_decoder), "enable-max-performance", true, NULL);

    /* Create a source GstBin to abstract this bin's content from the rest of the pipeline */
    gchar bin_name[16] = {};
    g_snprintf(bin_name, 15, "sink_%u", index);
    gchar streammux_pad_name[16] = {};
    g_snprintf(streammux_pad_name, 15, "sink_%u", index);
    g_print("--(SourceBin::create_src_bin)-- create_src_bin: video(%s), index(%i), klv_flag(%i) \n",
            bin.video_file, index, bin.klv_flag);
    g_print("--(SourceBin::create_src_bin)-- create_src_bin: pipeline (%s), streammux(%s), \n",
            gst_element_get_name(pipeline->pipeline), gst_element_get_name(pipeline->streammux));

    /* Add elements to bin */
    gst_bin_add_many(GST_BIN(pipeline->pipeline), bin.source, bin.tsparse, bin.tsdemux, NULL);
    gst_bin_add_many(GST_BIN(pipeline->pipeline), bin.codec_parser, bin.hardware_decoder, NULL);

    if (bin.klv_flag) {
        g_print("--(SourceBin::create_src_bin)-- creating klv elements for pipeline \n");
        g_object_set(G_OBJECT(bin.klv_sink), "dump", klv_dump, NULL);
        if (!bin.klv_parser || !bin.capsfilter || !bin.klv_sink) {
            g_printerr("--(SourceBin::create_src_bin: fail)-- not all klv stream elements were created. Exiting.\n");
            throw NULL;
        }
        gst_bin_add_many(GST_BIN(pipeline->pipeline), bin.klv_parser, bin.capsfilter, bin.klv_sink, NULL);
        if (!gst_element_link_many(bin.klv_parser, bin.capsfilter, bin.klv_sink, NULL)) {
            g_printerr("--(SourceBin::create_src_bin)-- BinLinkError <%s>: Failed to link (klv_parser, capsfilter, klv_sink) \n",
                       gst_element_get_name(pipeline->pipeline));
            throw NULL;
        }
    }

    /* Link elements to create directional data flow */
    if (!gst_element_link(bin.source, bin.tsdemux)) {
        g_printerr("--(SourceBin::create_src_bin:fail)-- BinLinkError <%s>: Failed to link (source, tsdemux) \n",
                   gst_element_get_name(pipeline->pipeline));
        throw NULL;
    }
    if (!gst_element_link_many(bin.codec_parser, bin.hardware_decoder, NULL)) {
        g_printerr("--(SourceBin::create_src_bin:fail)-- BinLinkError <%s>: Failed to link (codec_parser, hardware_decoder) \n",
                   gst_element_get_name(pipeline->pipeline));
        throw NULL;
    }

    /* Link video to nvstreammux */
    g_print("--(SourceBin::create_src_bin)-- Pipeline <%s>: Linking source_bin to stream-muxer <%s> \n",
            gst_element_get_name(pipeline->pipeline), gst_element_get_name(pipeline->streammux));
    GstPad *srcpad = gst_element_get_static_pad(bin.hardware_decoder, "src");
    if (!srcpad) {
        g_printerr("--(SourceBin::create_src_bin: fail)-- Pipeline <%s>: Failed to create bin.hardware_decoder \n",
                   gst_element_get_name(pipeline->pipeline));
        throw NULL;
    }
    GstPad *sinkpad = gst_element_get_request_pad(pipeline->streammux, streammux_pad_name);
    if (!sinkpad) {
        g_printerr("--(SourceBin::create_src_bin: fail)-- Pipeline <%s>: "
                   "Failed to create streammux request sink pad (%s) with file (%s) \n",
                   gst_element_get_name(pipeline->pipeline), streammux_pad_name, bin.video_file);
        throw NULL;
    }
    GstPadLinkReturn ret;
    ret = gst_pad_link(srcpad, sinkpad);
    if (GST_PAD_LINK_FAILED(ret)) {
        const gchar *ret_str = get_link_error(ret);
        g_printerr("--(SourceBin::create_src_bin: fail)-- PipelineLinkError <%s>: "
                   "Failed to link source bin to stream muxer: (%s::%s) with file (%s) \n",
                   gst_element_get_name(pipeline->pipeline),
                   gst_element_get_name(pipeline->streammux), gst_element_get_name(bin.hardware_decoder),
                   bin.video_file);
        g_printerr("--(SourceBin::create_src_bin: fail)-- link response: %s \n", ret_str);
        throw NULL;
    }
    // free pad references
    gst_object_unref(srcpad);
    gst_object_unref(sinkpad);

    /* store reference to tsdemux link pads */
    const gchar *video_element_name = gst_pad_get_name(bin.codec_parser);
    std::string tmp = "Ignored";
    const gchar *klv_element_name = tmp.c_str();
    if (bin.klv_flag) {
        klv_element_name = gst_pad_get_name(bin.klv_parser);
    }
    g_print("--(SourceBin::create_src_bin)-- tsdemux link: (video::%s), (klv::%s) \n", video_element_name, klv_element_name);
    bin.tsdemux_links = {bin.codec_parser, bin.klv_parser};
    g_print("--(SourceBin::create_src_bin)-- <%s> Created stream %d \n", gst_element_get_name(pipeline->pipeline), index);
    bin_container.push_back(bin);
    return;
};


void SourceBin::set_tsdemux_links(int index) {
    /** Set callback function for dynamic links */
    g_print("--(SourceBin::set_tsdemux_links)-- configuring bin %d \n", index);
    g_signal_connect(bin_container[index].tsdemux, "pad-added", G_CALLBACK(split_streams), &bin_container[index].tsdemux_links);
};


void Inference::setup_kafka() {

    /**
     * Instantiate kafka configuration for the class and send a `connection` message
     * to notify the consumer that the producer will start sending data.
     *
     */

    /* Load kafka library */
    char *error;
    printf("Load kafka library: %s \n", KAFKA_PROTO_PATH);
    void *so_handle = dlopen(KAFKA_PROTO_PATH, RTLD_LAZY);
    if (!so_handle) {
        error = dlerror();
        fprintf(stderr, "%s\n", error);
        printf("unable to open shared library\n");
        throw NULL;
    }

    /* Log setup of kafka producer and its configs */
    std::string kafka_connect_str = (std::string ) KAFKA_CONNECT_STR;
    std::size_t found = kafka_connect_str.find(";");
    std::string kafka_broker;
    while (found!=std::string::npos) {
        kafka_connect_str.replace(found, 1, ":");
        kafka_broker = kafka_connect_str.substr(0, found+5);
        g_print("\tRun this in another terminal: \n"
                "\t\t\tsudo docker exec -it kafkacat kafkacat -b %s -t %s -G %s -C \n",
                kafka_broker.c_str(), (char *) KAFKA_TOPIC, (char *) KAFKA_GROUP);
        // loop until finished ...
        kafka_connect_str = kafka_connect_str.substr(found+4, sizeof(kafka_connect_str)-(found + 4));
        found = kafka_connect_str.find(";", found + 1, 1);
    }

    /* Set up MsgApi connection (only need to do this once) */
    NvDsMsgApiHandle(*msgapi_connect_ptr)(char * connection_str, nvds_msgapi_connect_cb_t connect_cb, char * config_path);
    NvDsMsgApiErrorType(*msgapi_send_async_ptr)(NvDsMsgApiHandle h_ptr, char * topic, const uint8_t *payload, size_t nbuf, nvds_msgapi_send_cb_t send_callback, void * user_ptr);
    NvDsMsgApiErrorType(*msgapi_disconnect_ptr)(NvDsMsgApiHandle h_ptr);
    NvDsMsgApiErrorType(*msgapi_connection_signature_ptr)(char * connection_str, char * config_path, char * output_str, int max_len);

    *(void **) (&msgapi_connect_ptr) = dlsym(so_handle, "nvds_msgapi_connect");
    *(void **) (&msgapi_send_async_ptr) = dlsym(so_handle, "nvds_msgapi_send_async");
    *(void **) (&msgapi_disconnect_ptr) = dlsym(so_handle, "nvds_msgapi_disconnect");
    *(void **) (&msgapi_connection_signature_ptr) = dlsym(so_handle, "nvds_msgapi_connection_signature");

    if ((error = dlerror()) != NULL) {
        fprintf(stderr, "%s\n", error);
        throw NULL;
    }

    /* validate connection to NVDS api */
    char query_conn_signature[MAX_LEN];
    auto api_ret = msgapi_connection_signature_ptr(
            (char *) KAFKA_CONNECT_STR, (char *) KAFKA_CFG_FILE, query_conn_signature, MAX_LEN);
    if (api_ret == NVDS_MSGAPI_OK) {
        printf("\tconnection signature queried: < %s, %d > \n", query_conn_signature, NVDS_MSGAPI_OK);
    } else {
        printf("\tError querying connection signature string: ERROR_CODE(%d) \n", api_ret);
        throw NULL;
    }
    /* Kafka: set up broker (only need to do this once) */
    NvDsMsgApiHandle conn_handle = msgapi_connect_ptr(
            (char *) KAFKA_CONNECT_STR, (nvds_msgapi_connect_cb_t) msgapi_connect_cb, (char *) KAFKA_CFG_FILE);
    if (!conn_handle) {
        printf("\tConnect failed. Exiting\n");
        throw NULL;
    }

    /**
     * Store kafka instantiation into class container and send a start-up message
     * to notify the consumer that the producer is active.
     *
     */
    kafkaProducer producer = {
            .conn_handle = conn_handle, //
            .msgapi_connect_ptr = msgapi_connect_ptr, //
            .msgapi_send_async_ptr = msgapi_send_async_ptr, //
            .msgapi_disconnect_ptr = msgapi_disconnect_ptr, //
            .connection = false, //
            .topic = KAFKA_TOPIC, //
            .payload_count = 0, //
    };

    Inference::kafka_producer.push_back(producer);

    /* Send initialization key to the client to confirm one-way connection. */
    g_print("******************** KAFKA STARTUP (test start) ********************\n");

    int p_count = Inference::kafka_producer[0].payload_count;

    /** Sample Metadata from deepstream
      *
      *   NvDsBatchMeta:
      *       frame_number (76), ntp_ts (1632342727086032000), source_id (1)
      *   NvDsFrameMeta:
      *       unique_component_id (1), class_id (8), object_id (18446744073709551615)
      *       obj_label (Frigate, Halifax Class)
      *       confidence (0.859627), tracker_confidence (0.000000)
      *       bbox (xmin::402.00, xmax::684.00, ymin::470.81, ymax::567.00)
      *
      */
    Mist_bbox bbox = {402.00, 684.00, 470.81, 567.00};
    char *object_label = (char*) "Frigate, Halifax Class";
    DsMeta metadata;
    metadata.source_id = 1;
    metadata.frame_number = 0;
    metadata.ntp_timestamp = (guint64) 1632342727086032000;
    metadata.unique_component_id = 1;
    metadata.class_id = 8;
    metadata.object_id = (guint64) 18446744073709551615;
    metadata.confidence = 0.859627;
    metadata.tracker_confidence = 0.000000;
    metadata.bbox = bbox;
    metadata.obj_label = (gchar *) object_label;

    // msg converter
    static gchar *json_string;
    json_string = generate_schema_message(metadata, Inference::kafka_producer[0].payload_count);
    auto cb_ret = Inference::kafka_producer[0].msgapi_send_async_ptr(
            Inference::kafka_producer[0].conn_handle,
            (char *) Inference::kafka_producer[0].topic.c_str(),
            (const uint8_t *) json_string, strlen(json_string),
            NULL, NULL
            );

    if (cb_ret != NVDS_MSGAPI_OK) {
        printf("\t--(Initialization::kafka)-- ERROR: async msg failed [payload %d] \n", p_count);
        throw NULL;
    } else {
        printf("\t--(Initialization::kafka)-- async completed [payload %d] \n", p_count);
        Inference::kafka_producer[0].payload_count++;
    }
    g_print("******************** KAFKA STARTUP (test complete) ********************\n");
    Inference::kafka_producer[0].connection = true;
};


GstPadProbeReturn Inference::kafka_src_pad_buffer_probe (GstPad * pad, GstPadProbeInfo * info, gpointer producer_ptr) {

    /**
     *  A Probe callback function that produces a kafka payload.
     *  This function parses gstreamer metadata (NvDsFrameMeta) and transforms the payload
     *  into a json format for a kafka proto model.
     *
     */

    /* CB for kafka src pad probe */
    gchar *src_pad_name = gst_pad_get_name(pad);
    std::string probe_name = "nvinfer";
    kafkaProducer producer = *(kafkaProducer *) producer_ptr;

    GstBuffer *buf = (GstBuffer *) info->data;
    guint num_rects = 0;
    NvDsObjectMeta *obj_meta = NULL;
    NvDsMetaList * l_frame = NULL;
    NvDsMetaList * l_obj = NULL;
    NvDsDisplayMeta *display_meta = NULL;
    g_print("\n********  %s::kafka_src_pad_buffer_probe (osd::%d, %s::%d) (%s) ************* \n",
            (gchar *) probe_name.c_str(), rcvd_frame_number, (gchar *) probe_name.c_str(), kafka_frame_number, src_pad_name);

    /* deconstruct the NvDsBatchMeta structure in the gstreamer buffer */
    NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta (buf);
    guint num_frames_in_batch = batch_meta->num_frames_in_batch;
    g_print(">>>> NvDsBatchMeta \n");
    g_print("     num_frames_in_batch (%u) \n", num_frames_in_batch);
    int kafka_payload_counter = 0;

    for (l_frame = batch_meta->frame_meta_list; l_frame != NULL; l_frame = l_frame->next) {
        /* deconstruct the NvDsFrameMeta structure in the gstreamer buffer */
        NvDsFrameMeta *frame_meta = (NvDsFrameMeta *) (l_frame->data);
        guint source_id = frame_meta->source_id;
        int offset = 0;
        gint frame_number = frame_meta->frame_num;
        guint64 buf_pts = frame_meta->buf_pts;
        guint64 ntp_timestamp = frame_meta->ntp_timestamp;

        gint num_surfaces_per_frame = frame_meta->num_surfaces_per_frame;
        guint surface_index = frame_meta->surface_index;
        guint num_obj_meta = frame_meta->num_obj_meta;
        gboolean bInferDone = frame_meta->bInferDone;
//        gint64 *misc_frame_info = frame_meta->misc_frame_info;

        /* deconstruct the NvDsObjectMeta structure in the gstreamer buffer */
        int objects_detected = 0;
        for (l_obj = frame_meta->obj_meta_list; l_obj != NULL; l_obj = l_obj->next) {
            obj_meta = (NvDsObjectMeta * )(l_obj->data);

            /* standard object metadata fields */
            gint unique_component_id = obj_meta->unique_component_id;
            gint class_id = obj_meta->class_id;
            guint64 object_id = obj_meta->object_id;
            NvDsComp_BboxInfo detector_bbox_info = obj_meta->detector_bbox_info;
            NvDsComp_BboxInfo tracker_bbox_info = obj_meta->tracker_bbox_info;
            gfloat confidence = obj_meta->confidence;
            gfloat tracker_confidence = obj_meta->tracker_confidence;
            NvOSD_RectParams rect_params = obj_meta->rect_params;
            gchar *obj_label = obj_meta->obj_label;
            gint64 *misc_obj_info = obj_meta->misc_obj_info;

            objects_detected++;

            /**
             * send kafka payload
             *
             * __header info__
             * source_id (input source it came from)
             * frame_number
             * ntp_timestamp
             *
             * __detection data__
             * unique_component_id (?)
             * class_id (detector class output as integer)
             * object_id (object id as integer ..?)
             * confidence (TODO: filtering is required)
             * tracker_confidence
             * rect_params (bbox)
             * obj_label (human readable)
             *
             *
             */

            // setup struct to store data
            float xmin = rect_params.left;
            float xmax = rect_params.left + rect_params.width;
            float ymin = rect_params.top;
            float ymax = rect_params.top + rect_params.height;
            Mist_bbox bbox = {xmin, xmax, ymin, ymax};

            DsMeta metadata;
            metadata.source_id = source_id;
            metadata.frame_number = frame_number;
            metadata.ntp_timestamp = ntp_timestamp;
            metadata.unique_component_id = unique_component_id;
            metadata.class_id = class_id;
            metadata.object_id = object_id;
            metadata.confidence = confidence;
            metadata.tracker_confidence = tracker_confidence;
            metadata.bbox = bbox;
            metadata.obj_label = obj_label;

            g_print(">>>> (object # %d): \n", objects_detected);
            g_print("     NvDsBatchMeta\n\t\t\tframe_number (%d), ntp_ts (%lu), source_id (%u) \n",
                    metadata.frame_number, metadata.ntp_timestamp, metadata.source_id);
            g_print("     NvDsFrameMeta\n\t\t\tunique_component_id (%d), class_id (%d), object_id (%lu) \n\t\t\tobj_label (%s) \n",
                    metadata.unique_component_id, metadata.class_id, metadata.object_id, metadata.obj_label);
            g_print("\t\t\tconfidence (%f), tracker_confidence (%f) \n",
                    metadata.confidence, metadata.tracker_confidence);
            g_print("\t\t\tbbox (xmin::%2.2f, xmax::%2.2f, ymin::%2.2f, ymax::%2.2f)  \n",
                    metadata.bbox.xmin, metadata.bbox.xmax, metadata.bbox.ymin, metadata.bbox.ymax);

            // msg converter
            static gchar *json_string;
            json_string = generate_schema_message(metadata, producer.payload_count);
            // send using kafka proto producer
            auto cb_ret = producer.msgapi_send_async_ptr(producer.conn_handle, (char *) producer.topic.c_str(), (const uint8_t *) json_string, strlen(json_string), NULL, NULL);
            if (cb_ret != NVDS_MSGAPI_OK) {
                printf("--(kafka::producer)-- ERROR: (frame %d) async msg failed [payload %d] \n", kafka_frame_number, producer.payload_count);
            } else {
                // Increment counter on success
                printf("--(kafka::producer)-- (frame %d) async completed [payload %d] \n", kafka_frame_number, producer.payload_count);
                producer.payload_count++;
            }

        }

    }
    /* update class counter to track frames received */
    kafka_frame_number++;
    return GST_PAD_PROBE_OK;
};


void Inference::setup_pipeline(int num_sources, int index, const gchar * pgie_yolo) {

    /**
     * Create InferencePipeline struct to hold the main pipeline references.
     * This is the body of the pipeline and all sources will are linked to nvstreammux.
     *
     */
    /* Create shared pipeline elements between streams */
    InferencePipeline pipeline = {
            (GstElement *) gst_pipeline_new("Yolo-MultiUriSrc-Kafka"),
            (GstElement *) gst_element_factory_make("nvstreammux", g_strdup_printf("stream-muxer_%i", index)),
            (GstElement *) gst_element_factory_make("nvinfer", g_strdup_printf("pgie_%i", index)),
            (GstElement *) gst_element_factory_make("nvtracker", g_strdup_printf("nvtracker_%i", index)),
            (GstElement *) gst_element_factory_make("nvmultistreamtiler", g_strdup_printf("tiler_%i", index)),
            (GstElement *) gst_element_factory_make("nvvideoconvert", g_strdup_printf("nvvidconv_%i", index)),
            (GstElement *) gst_element_factory_make("nvdsosd", g_strdup_printf("nvosd_%i", index)),
            (GstElement *) gst_element_factory_make("nvegltransform", g_strdup_printf("transform_%i", index)),
            (GstElement *) gst_element_factory_make("nveglglessink", g_strdup_printf("sink_%i", index)),
            (int) num_sources,
    };
    if (!pipeline.pipeline || !pipeline.streammux) {
        g_printerr("--(Inference::setup_pipeline)-- FAIL: creation of pipeline or streammux failed. Exiting.\n");
        exit(-1);
    }
    if (!pipeline.pgie || !pipeline.tiler || !pipeline.nvvidconv || !pipeline.nvosd ||
        !pipeline.transform || !pipeline.sink) {
        g_printerr("--(Inference::setup_pipeline)-- FAIL: not all video stream elements were created. Exiting.\n");
        exit(-1);
    }

    /**
     *  Set the element properties
     *
     */
    g_object_set(G_OBJECT(pipeline.streammux),
                 "batch-size", pipeline.num_sources,
                 "config-file-path", "ds_configs/streammux_config.txt",
                 NULL
                 );

    g_object_set(G_OBJECT(pipeline.pgie), "batch-size", pipeline.num_sources, NULL);
    g_object_set(G_OBJECT(pipeline.pgie), "config-file-path", pgie_yolo, NULL);

    g_object_set(G_OBJECT(pipeline.tracker),
                 "qos", 1,
                 "tracker-width", 640,  // MUXER_OUTPUT_WIDTH
                 "tracker-height", 384, // MUXER_OUTPUT_HEIGHT
                 "gpu-id", 0,
                 "ll-lib-file", "/opt/nvidia/deepstream/deepstream/lib/libnvds_mot_klt.so",
                 "enable-batch-process", 1,
                 "enable-past-frame", 1,
                 "display-tracking-id", 1,
                 NULL);

    /* TILER: properties set to match the number of input sources */
    guint tiler_rows, tiler_columns;
    tiler_rows = (guint) sqrt(pipeline.num_sources);
    tiler_columns = (guint) ceil(1.0 * pipeline.num_sources / tiler_rows);
    g_object_set(G_OBJECT(pipeline.tiler),
                 "rows", tiler_rows,
                 "columns", tiler_columns,
                 "width", TILED_OUTPUT_WIDTH,
                 "height", TILED_OUTPUT_HEIGHT,
                 NULL
                 );

    /* OSD: uses HW process-mode (jetson only) and display-text to true */
    g_object_set(G_OBJECT(pipeline.nvosd), "process-mode", 2, "display-text", true, NULL);

    /* SINK: don't handle quality-of-service events and disable element synchronization to pipeline */
    g_object_set(G_OBJECT(pipeline.sink), "qos", 0, "sync", 0, NULL);

    /**
     *  Set Probe callback functions on element pads
     *
     */

    /* Set kafka producer on pgie src pad (nvinfer) */
    GstPad *kafka_producer_pad = gst_element_get_static_pad (pipeline.pgie, "src");
    if (!kafka_producer_pad) {
        g_printerr("--(Inference::setup_pipeline %d)-- FAIL: Unable to src pad from nvinfer (pgie) (%s) \n",
                   index, gst_element_get_name(pipeline.pgie));
        throw NULL;
    } else {
        gst_pad_add_probe (kafka_producer_pad,
                           GST_PAD_PROBE_TYPE_BUFFER,
                           &Inference::kafka_src_pad_buffer_probe,
                           (gpointer) &Inference::kafka_producer[index],
                           NULL
                           );
    }
    gst_object_unref (kafka_producer_pad);

    /**
     * Add elements to bin
     *
     */
    gst_bin_add(GST_BIN(pipeline.pipeline), pipeline.streammux);
    if (pipeline.num_sources != 1) {
        gst_bin_add(GST_BIN(pipeline.pipeline), pipeline.tiler);
    }
    gst_bin_add_many(GST_BIN(pipeline.pipeline), pipeline.pgie, pipeline.nvvidconv, pipeline.nvosd,
                     pipeline.transform, pipeline.sink, NULL);
    // store pipeline references
    Inference::pipeline_container.push_back(pipeline);
    g_print("--(Inference::setup_pipeline)-- completed main pipeline config (%d) \n", index);
}
