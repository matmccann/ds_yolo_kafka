
#ifndef MIST_META_H
#define MIST_META_H

#include <string>
#include <gst/gst.h>
#include <glib.h>
#include <stdlib.h>
#include "gstnvdsmeta.h"
//#include "nvds_msgapi.h"
//#include "mist_msgconv.h"

#ifdef __cplusplus
extern "C"
{
#endif


/* NvDsMeta configs */
#define USER_META_SIZE 16


//GST_DEBUG_CATEGORY (NVDS_APP);


using namespace std;

typedef struct _Mist_bbox {
    float xmin;     // xmin = rect_params.left,
    float xmax;     // xmax = rect_params.left + obj_meta.rect_params.width,
    float ymin;     // ymin = rect_params.top,
    float ymax;     // ymax = rect_params.top + obj_meta.rect_params.height,
} Mist_bbox;

typedef struct _DsMeta {
    guint source_id;
    gint frame_number;
    guint64 ntp_timestamp;
    gint unique_component_id;
    gint class_id;
    guint64 object_id;
    gfloat confidence;
    gfloat tracker_confidence;
    Mist_bbox bbox;
    gchar *obj_label;
} DsMeta;

typedef struct _CustomMeta
{
    gchar size;
    gint frame;
    std::string description;
    char data[16][20];
} CustomMeta;

void *set_metadata_ptr(int, int);
static gpointer copy_user_meta(gpointer data, gpointer user_data);
static void release_user_meta(gpointer data, gpointer user_data);


#ifdef __cplusplus
}
#endif
#endif //MIST_META_H
