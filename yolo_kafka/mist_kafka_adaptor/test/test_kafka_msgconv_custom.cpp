#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "nvds_msgapi.h"
/* msgconv libraries*/
#include "mist_msgconv.h"


/* Kafka library path and configs */
#define KAFKA_PROTO_PATH "/opt/nvidia/deepstream/deepstream/lib/mist_kafka_proto.so"
#define MSGCONV_PROTO_PATH "/home/nvidia/code/seamist/yolo_kafka/mist_msgconv/mist_msgconv.so"
#define KAFKA_CFG_FILE "cfg_kafka.txt"
//connection string format: host;port
#define KAFKA_CONNECT_STR "172.22.0.3;9092"
#define KAFKA_TOPIC "ir"
#define KAFKA_GROUP "seamist"
#define MAX_LEN 256


using namespace std;
gchar *generate_schema_message(DsMeta metadata, int frame);

void sample_msgapi_connect_cb(NvDsMsgApiHandle *h_ptr, NvDsMsgApiEventType ds_evt) {}

int g_cb_count = 0;
int consumed_cnt = 0;

void test_send_cb(void *user_ptr, NvDsMsgApiErrorType completion_flag) {
    // printf("async send complete (from test_send_cb)\n");
    if (completion_flag == NVDS_MSGAPI_OK)
        printf("%s successfully \n", (char *) user_ptr);
    else
        printf("%s with failure\n", (char *) user_ptr);
    g_cb_count++;
}


int main() {
    char *error;

    /* Load kafka library */
    printf("Load kafka library: %s \n", KAFKA_PROTO_PATH);
    void *so_handle = dlopen(KAFKA_PROTO_PATH, RTLD_LAZY);
    if (!so_handle) {
        error = dlerror();
        fprintf(stderr, "%s\n", error);
        printf("unable to open mist_kafka_proto shared library\n");
        exit(-1);
    }

    void *so_msgconv_handle = dlopen(MSGCONV_PROTO_PATH, RTLD_LAZY);
    if (!so_msgconv_handle) {
        error = dlerror();
        fprintf(stderr, "%s\n", error);
        printf("unable to open mist_msgconv shared library\n");
        exit(-1);
    }

    printf("\tRun this in another terminal: \n"
           "\t\t\tsudo docker exec -it kafkacat kafkacat -b '172.22.0.3:9092' -t 'ir' -G seamist -C \n");
    printf("\nStarting... in 5 secs\n");
    sleep(5);

    /* Set up MsgApi connection (only need to do this once) */
    NvDsMsgApiHandle conn_handle;
    NvDsMsgApiHandle(*msgapi_connect_ptr)(char * connection_str, nvds_msgapi_connect_cb_t connect_cb, char * config_path);
    NvDsMsgApiErrorType(*msgapi_send_ptr)(NvDsMsgApiHandle conn, char * topic, const uint8_t *payload, size_t nbuf);
    NvDsMsgApiErrorType(*msgapi_send_async_ptr)(
            NvDsMsgApiHandle h_ptr,
            char * topic,
    const uint8_t *payload,
            size_t nbuf,
            nvds_msgapi_send_cb_t send_callback,
            void * user_ptr
    );
    NvDsMsgApiErrorType(*msgapi_disconnect_ptr)(NvDsMsgApiHandle h_ptr);
    char *(*msgapi_getversion_ptr)(void);
    char *(*msgapi_get_protocol_name_ptr)(void);
    NvDsMsgApiErrorType(*msgapi_connection_signature_ptr)(char * connection_str, char * config_path, char * output_str, int max_len);

    // load msgconv function
//    gchar *(*generate_schema_message)(DsMeta metadata, int frame);
//    *(void **) (&generate_schema_message) = dlsym(so_msgconv_handle, "generate_schema_message");

    // load kafka functions
    *(void **) (&msgapi_connect_ptr) = dlsym(so_handle, "nvds_msgapi_connect");
    *(void **) (&msgapi_send_async_ptr) = dlsym(so_handle, "nvds_msgapi_send_async");
    *(void **) (&msgapi_disconnect_ptr) = dlsym(so_handle, "nvds_msgapi_disconnect");
    *(void **) (&msgapi_getversion_ptr) = dlsym(so_handle, "nvds_msgapi_getversion");
    *(void **) (&msgapi_get_protocol_name_ptr) = dlsym(so_handle, "nvds_msgapi_get_protocol_name");
    *(void **) (&msgapi_connection_signature_ptr) = dlsym(so_handle, "nvds_msgapi_connection_signature");

    if ((error = dlerror()) != NULL) {
        fprintf(stderr, "%s\n", error);
        exit(-1);
    }
    printf("\tAdapter protocol=%s , version=%s\n", msgapi_get_protocol_name_ptr(), msgapi_getversion_ptr());

    char query_conn_signature[MAX_LEN];
    auto api_ret = msgapi_connection_signature_ptr(
            (char *) KAFKA_CONNECT_STR,
            (char *) KAFKA_CFG_FILE,
            query_conn_signature,
            MAX_LEN
    );

    if (api_ret == NVDS_MSGAPI_OK) {
        printf("\tconnection signature queried: < %s, %d > \n", query_conn_signature, NVDS_MSGAPI_OK);
    } else {
        printf("\tError querying connection signature string: ERROR_CODE(%d) \n", api_ret);
        exit(-1);
    }
    /* Kafka: set up broker (only need to do this once) */
    conn_handle = msgapi_connect_ptr(
            (char *) KAFKA_CONNECT_STR,
            (nvds_msgapi_connect_cb_t) sample_msgapi_connect_cb,
            (char *) KAFKA_CFG_FILE
    );
    if (!conn_handle) {
        printf("\tConnect failed. Exiting\n");
        exit(-1);
    }

    printf("\n\n*****\nkafka payload test ... \n");

    /* Kafka:  producer messages
     * call generate_schema_message() and  msgapi_send_async_ptr() as needed.
     *
     * view mist_msgconv/README.md and mist_msg_conv/Makefile for instructions for linking
     *
     * */
    static gchar *json_string;
    int num_message = 5;
    printf("Kafka sending %d async messages ...\n", num_message);
    for (int i = 0; i < num_message; i++) {
        char display_str[100];
        snprintf(&(display_str[0]), 100, "Async send [%d] complete", i);

        /** Sample Metadata from deepstream
          *
          *   NvDsBatchMeta:
          *       frame_number (76), ntp_ts (1632342727086032000), source_id (1)
          *   NvDsFrameMeta:
          *       unique_component_id (1), class_id (8), object_id (18446744073709551615)
          *       obj_label (Frigate, Halifax Class)
          *       confidence (0.859627), tracker_confidence (0.000000)
          *       bbox (xmin::402.00, xmax::684.00, ymin::470.81, ymax::567.00)
          *
          */
        Mist_bbox bbox = {402.00, 684.00, 470.81, 567.00};
        char *object_label = (char*) "Frigate, Halifax Class";

        DsMeta metadata;
        metadata.source_id = 1;
        metadata.frame_number = 0;
        metadata.ntp_timestamp = (guint64) 1632342727086032000;
        metadata.unique_component_id = 1;
        metadata.class_id = 8;
        metadata.object_id = (guint64) 18446744073709551615;
        metadata.confidence = 0.859627;
        metadata.tracker_confidence = 0.000000;
        metadata.bbox = bbox;
        metadata.obj_label = (gchar *) object_label;

        json_string = generate_schema_message(metadata, i);
        auto cb_ret = msgapi_send_async_ptr(conn_handle, (char *) KAFKA_TOPIC, (const uint8_t *) json_string, strlen(json_string), test_send_cb, &(display_str[0]));
        if (cb_ret != NVDS_MSGAPI_OK) {
            printf("\t--(kafka)-- ERROR: async msg failed [%d] \n", i);
        } else {
            printf("\t--(kafka)-- async msg sent [%d] \n", i);
        }
    }

    /* Kafka: shut down but give time for the async call backs to display */
    printf("\n\n*****\nDisconnecting... in 3 secs\n");
    printf("Refer to nvds log file for log output: \n\tcat /tmp/nvds/ds.log \n\n");
    sleep(3);
    msgapi_disconnect_ptr(conn_handle);
}
