#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include "nvds_msgapi.h"

/* MODIFY: to reflect your own path */
#define KAFKA_PROTO_PATH "/opt/nvidia/deepstream/deepstream/lib/mist_kafka_proto.so"
#define KAFKA_CFG_FILE "cfg_kafka.txt"
//connection string format: host;port
#define KAFKA_CONNECT_STR "172.22.0.3;9092"
#define KAFKA_TOPIC "ir"
#define KAFKA_GROUP "seamist"
#define MAX_LEN 256

using namespace std;

//const char SEND_MSG[] = "sample-message";
const char SEND_MSG[] = "{ \
   \"messageid\" : \"84a3a0ad-7eb8-49a2-9aa7-104ded6764d0_c788ea9efa50\", \
   \"mdsversion\" : \"1.0\", \
   \"@timestamp\" : \"\", \
   \"place\" : { \
    \"id\" : \"1\", \
    \"name\" : \"HQ\", \
    \"type\" : \"building/garage\", \
    \"location\" : { \
      \"lat\" : 0, \
      \"lon\" : 0, \
      \"alt\" : 0 \
    }, \
    \"aisle\" : { \
      \"id\" : \"C_126_135\", \
      \"name\" : \"Lane 1\", \
      \"level\" : \"P1\", \
      \"coordinate\" : { \
        \"x\" : 1, \
        \"y\" : 2, \
        \"z\" : 3 \
      } \
     }\
    },\
   \"sensor\" : { \
    \"id\" : \"10_110_126_135_A0\", \
    \"type\" : \"Camera\", \
    \"description\" : \"Aisle Camera\", \
    \"location\" : { \
      \"lat\" : 0, \
      \"lon\" : 0, \
      \"alt\" : 0 \
    }, \
    \"coordinate\" : { \
      \"x\" : 0, \
      \"y\" : 0, \
      \"z\" : 0 \
     } \
    } \
   }";

void sample_msgapi_connect_cb(NvDsMsgApiHandle *h_ptr, NvDsMsgApiEventType ds_evt) {}

int g_cb_count = 0;
int consumed_cnt = 0;

void test_send_cb(void *user_ptr, NvDsMsgApiErrorType completion_flag) {
    // printf("async send complete (from test_send_cb)\n");
    if (completion_flag == NVDS_MSGAPI_OK)
        printf("%s successfully \n", (char *) user_ptr);
    else
        printf("%s with failure\n", (char *) user_ptr);
    g_cb_count++;
}

int main() {

    /* Setup kafka connection */
    NvDsMsgApiHandle conn_handle;
    NvDsMsgApiHandle(*msgapi_connect_ptr)(
            char * connection_str, nvds_msgapi_connect_cb_t connect_cb, char * config_path);
    NvDsMsgApiErrorType(*msgapi_send_ptr)(
            NvDsMsgApiHandle conn, char * topic, const uint8_t *payload, size_t nbuf);
    NvDsMsgApiErrorType(*msgapi_send_async_ptr)(
            NvDsMsgApiHandle h_ptr, char * topic, const uint8_t *payload,
            size_t nbuf, nvds_msgapi_send_cb_t send_callback, void * user_ptr);
    NvDsMsgApiErrorType(*msgapi_disconnect_ptr)(NvDsMsgApiHandle h_ptr);
    char *(*msgapi_getversion_ptr)(void);
    char *(*msgapi_get_protocol_name_ptr)(void);
    NvDsMsgApiErrorType(*msgapi_connection_signature_ptr)(
            char * connection_str, char * config_path, char * output_str, int max_len);

    void *so_handle = dlopen(KAFKA_PROTO_PATH, RTLD_LAZY);
    char *error;

    printf("Refer to nvds log file for log output\n");
    if (!so_handle) {
        error = dlerror();
        fprintf(stderr, "%s\n", error);

        printf("unable to open shared library\n");
        exit(-1);
    }

    *(void **) (&msgapi_connect_ptr) = dlsym(so_handle, "nvds_msgapi_connect");
    *(void **) (&msgapi_send_async_ptr) = dlsym(so_handle, "nvds_msgapi_send_async");
    *(void **) (&msgapi_disconnect_ptr) = dlsym(so_handle, "nvds_msgapi_disconnect");
    *(void **) (&msgapi_getversion_ptr) = dlsym(so_handle, "nvds_msgapi_getversion");
    *(void **) (&msgapi_get_protocol_name_ptr) = dlsym(so_handle, "nvds_msgapi_get_protocol_name");
    *(void **) (&msgapi_connection_signature_ptr) = dlsym(so_handle, "nvds_msgapi_connection_signature");

    if ((error = dlerror()) != NULL) {
        fprintf(stderr, "%s\n", error);
        exit(-1);
    }
    printf("Adapter protocol=%s , version=%s\n", msgapi_get_protocol_name_ptr(), msgapi_getversion_ptr());

    char query_conn_signature[MAX_LEN];
    if (msgapi_connection_signature_ptr((char *) KAFKA_CONNECT_STR, (char *) KAFKA_CFG_FILE, query_conn_signature,
                                        MAX_LEN) != NVDS_MSGAPI_OK) {
        printf("Error querying connection signature string\n");
    }
    printf("connection signature queried: < %s > \n", query_conn_signature);

    // set kafka broker appropriately
    conn_handle = msgapi_connect_ptr((char *) KAFKA_CONNECT_STR, (nvds_msgapi_connect_cb_t) sample_msgapi_connect_cb,
                                     (char *) KAFKA_CFG_FILE);
    if (!conn_handle) {
        printf("Connect failed. Exiting\n");
        exit(-1);
    }

    /* Sending kafka messages */
    /*
     * needs:
     *      msgapi_send_async_ptr
     *      conn_handle
     *      SEND_MSG
     *      test_send_cb
     * */
    int num_message = 5;
    char display_str[100];
    printf("Kafka sending %d async messages ...\n", num_message);

    for (int i = 0; i < num_message; i++) {
        snprintf(&(display_str[0]), 100, "Async send [%d] complete", i);
        auto cb_ret = msgapi_send_async_ptr(conn_handle, (char *) KAFKA_TOPIC, (const uint8_t *) SEND_MSG, strlen(SEND_MSG), test_send_cb, &(display_str[0]));
        if (cb_ret != NVDS_MSGAPI_OK) {
            printf("\t--(kafka)-- ERROR: async msg failed [%d] \n", i);
        } else {
            printf("\t--(kafka)-- async msg sent [%d] \n", i);
        }

    }

    printf("Disconnecting... in 3 secs\n");
    sleep(3);
    msgapi_disconnect_ptr(conn_handle);
}
