
#include "mist_meta.h"

/**
 * Custom metadata structures
 *
 */

char *metatype = (char*)("NVIDIA.NVINFER.USER_META");
auto NVDS_USER_FRAME_META_EXAMPLE = nvds_get_user_meta_type(metatype);

gint frame_meta_number = 0;
gint frame_meta_number_rcvd = 0;
gint batch_meta_number = 0;
gint batch_meta_number_rcvd = 0;

CustomMeta custom_metadata_struct = {
        .size = 16,
        .frame = 0,
        .description = "string",
};

// *****************************************************
/* Example of setting user_meta_data in in frame to a CustomMetadata struct */
void *set_metadata_ptr(int batch_num, int frame_num)
{
    int i = 0;
    CustomMeta *user_metadata_test = (CustomMeta*)g_malloc0(sizeof(CustomMeta));
    user_metadata_test->size = 16;
    user_metadata_test->frame = custom_metadata_struct.frame;
    user_metadata_test->description = "klv";
    const char *klv_values[16] = {
            "timestamp", "ts_epoch", "klv_frame_number", "acft_hdg",
            "sensor_lat", "sensor_lon", "sensor_alt", "hfov",
            "vfov", "sensor_az", "slant_range", "target_width",
            "center_lat", "center_lon", "center_elev", "sensor_id",
    };
    g_print("\n**************** [Batch (%d) create frame_meta (%d) ] nvinfer src pad\n", batch_num, frame_num);
    for(i = 0; i < custom_metadata_struct.size; i++) {
        memcpy(user_metadata_test->data[i], klv_values[i], 20);
//        g_print("frame_user_meta_data [%d] = %s\n", i, user_metadata_test->data[i]);
    }
    custom_metadata_struct.frame += 1;
    return (void *)user_metadata_test;
}

/* copy function set by user. "data" holds a pointer to NvDsUserMeta*/
static gpointer copy_user_meta(gpointer data, gpointer user_data)
{
    NvDsUserMeta *user_meta = (NvDsUserMeta *)data;
    CustomMeta *src_user_metadata = (CustomMeta*)user_meta->user_meta_data;
    CustomMeta *dst_user_metadata = (CustomMeta*)g_malloc0(sizeof(CustomMeta));
    memcpy(dst_user_metadata, src_user_metadata, sizeof(CustomMeta));
    return (gpointer)dst_user_metadata;
}

/* release function set by user. "data" holds a pointer to NvDsUserMeta*/
static void release_user_meta(gpointer data, gpointer user_data) {
    NvDsUserMeta *user_meta = (NvDsUserMeta *) data;
    if(user_meta->user_meta_data) {
        g_free(user_meta->user_meta_data);
        user_meta->user_meta_data = NULL;
    }
}

static GstPadProbeReturn custom_medata_read_probe (GstPad * pad, GstPadProbeInfo * info, gpointer u_data) {

    GstBuffer *buf = (GstBuffer *) info->data;
    NvDsMetaList * l_frame = NULL;
    NvDsMetaList * l_user_meta = NULL; // for parsing user_data from frame_meta_list
    // NvDsUserMetaList * l_batch_meta_user = NULL; // for parsing user_data from batch_meta_list
    NvDsUserMeta *user_meta = NULL;
    CustomMeta *user_meta_data_custom = NULL; // for parsing custom metadata structure

    int i = 0;

    NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta (buf);
    for (l_frame = batch_meta->frame_meta_list; l_frame != NULL;
         l_frame = l_frame->next) {

        /* Validate user meta attached at the frame level */
        NvDsFrameMeta *frame_meta = (NvDsFrameMeta *) (l_frame->data);
        for (l_user_meta = frame_meta->frame_user_meta_list; l_user_meta != NULL; l_user_meta = l_user_meta->next) {

            user_meta = (NvDsUserMeta *) (l_user_meta->data);
            user_meta_data_custom = (CustomMeta *)user_meta->user_meta_data;

            if(user_meta->base_meta.meta_type == NVDS_USER_FRAME_META_EXAMPLE)
            {
                g_print("\n************ [Batch (%d) rvd frame_meta (%d) ] osd sink pad\n", batch_meta_number_rcvd, frame_meta_number_rcvd);
                for(i = 0; i < custom_metadata_struct.size; i++) {
                    g_print("frame_user_meta_data [%d] %s \n", i, user_meta_data_custom->data[i]);
                }
                g_print("\n");
            }
        }
        frame_meta_number_rcvd++;
    }
    batch_meta_number_rcvd ++;
    return GST_PAD_PROBE_OK;
}


static GstPadProbeReturn custom_medata_add_probe (GstPad * pad, GstPadProbeInfo * info, gpointer u_data) {

    GstBuffer *buf = (GstBuffer *) info->data;
    NvDsMetaList * l_frame = NULL;
    NvDsUserMeta *user_meta = NULL;
    NvDsMetaType user_meta_type = NVDS_USER_FRAME_META_EXAMPLE;
    g_print("***********************");
    g_print("NVDS_USER_FRAME_META_EXAMPLE: %d", NVDS_USER_FRAME_META_EXAMPLE);

    NvDsBatchMeta *batch_meta = gst_buffer_get_nvds_batch_meta (buf);
    for (l_frame = batch_meta->frame_meta_list; l_frame != NULL;
         l_frame = l_frame->next) {
        NvDsFrameMeta *frame_meta = (NvDsFrameMeta *) (l_frame->data);

        /* Acquire NvDsUserMeta user meta from pool */
        user_meta = nvds_acquire_user_meta_from_pool(batch_meta);

        /* Set NvDsUserMeta below */

        user_meta->user_meta_data = (void *)set_metadata_ptr(batch_meta_number, frame_meta_number);
        user_meta->base_meta.copy_func = (NvDsMetaCopyFunc)copy_user_meta;

        frame_meta_number++;
        user_meta->base_meta.meta_type = user_meta_type;
        user_meta->base_meta.release_func = (NvDsMetaReleaseFunc)release_user_meta;

        /* We want to add NvDsUserMeta to frame level */
        nvds_add_user_meta_to_frame(frame_meta, user_meta);
    }
    batch_meta_number++;
    return GST_PAD_PROBE_OK;
}

/**
 * Example usage
 *
 *  infer_src_pad = gst_element_get_static_pad (pgie, "src");
 *  if (!infer_src_pad)
 *    g_print ("Unable to get source pad\n");
 *  else
 *    gst_pad_add_probe (infer_src_pad, GST_PAD_PROBE_TYPE_BUFFER, custom_medata_add_probe, NULL, NULL);
 *  gst_object_unref (infer_src_pad);
 *
 *
 *  osd_sink_pad = gst_element_get_static_pad (nvosd, "sink");
 *   if (!osd_sink_pad)
 *     g_print ("Unable to get sink pad\n");
 *  else
 *    gst_pad_add_probe (osd_sink_pad, GST_PAD_PROBE_TYPE_BUFFER, custom_medata_read_probe, NULL, NULL);
 *  gst_object_unref (osd_sink_pad);
 *
 *
 *
 *
 *
 *
 *
 *
 */


