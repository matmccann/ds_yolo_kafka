# Mist Message Converter (i.e. the kafka payload generator)

Refer to the DeepStream SDK documentation for a description of the plugin.
- customized for mist

--------------------------------------------------------------------------------

# Creating shared library
__Pre-requisites__

```bash
sudo apt-get install -y libglib2.0-dev libjson-glib-dev uuid-dev
```

__Compiling and installing the plugin__

```bash
make && sudo make install 
```

# Testing
```bash
g++ -o example nvmsgconv.cpp  -pthread -I/usr/include/gstreamer-1.0 -I/usr/include/json-glib-1.0 -I/usr/include/glib-2.0 -I/usr/lib/aarch64-linux-gnu/glib-2.0/include -I/usr/include/uuid -lgstreamer-1.0 -ljson-glib-1.0 -lgio-2.0 -lgobject-2.0 -lglib-2.0 -luuid -I/opt/nvidia/deepstream/deepstream/sources/includes -rdynamic -L/opt/nvidia/deepstream/deepstream-5.1/lib -ldl -Wl,-rpath=/opt/nvidia/deepstream/deepstream-5.1/lib
```
