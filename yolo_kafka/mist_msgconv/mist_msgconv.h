
/**
 * @file
 * <b>Mist DeepStream: Message Schema Generation Library Interface</b>
 *
 * @b Description: This file specifies the NVIDIA DeepStream message schema generation library interface.
 */

#ifndef MISTSGCONV_H_
#define MISTGCONV_H_

#include <stdio.h>
#include <dlfcn.h>
#include <string.h>
#include <stdlib.h>
#include <json-glib/json-glib.h>
#include <glib.h>
#include <inttypes.h>
#include "mist_meta.h"
#include "nvdsmeta_schema.h"

#ifdef __cplusplus
extern "C"
{
#endif

/**
 * This function generates a microsecond epoch timestamp .
 *
 * @param[in] void placeholder for buffer metadata.
 */
int64_t generate_ts_epoch(void);

/**
 * This function generates the json field for 'sensor'.
 *
 * @param[in] void placeholder for buffer metadata.
 */
JsonObject *generate_sensor_object(guint source_id);

/**
 * This function generates the json field for 'klv'.
 *
 * @param[in] void placeholder for buffer metadata.
 */
JsonObject *generate_klv_object(void);

/**
 * This function generates the kafka payload as a json string
 *
 * @param[in] void placeholder for buffer metadata.
 */
gchar *generate_schema_message(DsMeta metadata, int frame);

/**
 *
 * @param metadata
 * @return
 */
JsonObject *generate_ds_object(DsMeta metadata);

#ifdef __cplusplus
}
#endif
#endif /* MISTSGCONV_H_ */
