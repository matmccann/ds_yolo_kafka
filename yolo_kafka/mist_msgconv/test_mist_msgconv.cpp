#include <stdio.h>
#include "mist_msgconv.h"
#include <string.h>

gchar *generate_sample_schema_message(int);

int main() {
    static gchar *json_string;
    int num_message = 1;
    printf("Kafka sending %d async messages ...\n", num_message);
    json_string = generate_schema_message(1);
    printf("message: \n %s \n", json_string);
    printf("Success. Exiting ... \n");
}
