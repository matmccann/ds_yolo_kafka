#!/bin/bash

# Example usage: sudo -H ./run.sh
export NAME="[run.sh] "
export HOME_DIR=$(pwd)

set -eE -o functrace
failure() {
  local lineno=$1
  local msg=$2
  echo "${NAME} Failed at $lineno: $msg"
}
trap '${NAME} failure ${LINENO} "$BASH_COMMAND"' ERR

echo "${NAME} Build docker containers"
docker network prune -f
docker-compose up -d

echo "${NAME} In another terminal, run this command to set up your kafka listener"

printf "\n   currently available broker topics:    sudo docker exec -it kafkacat kafkacat -L -b 172.22.0.3:9092"
printf "\n   listen to a topic:                    sudo docker exec -it kafkacat kafkacat -b 172.22.0.3:9092 -t 'test' -C \n\n"
echo "${NAME} Then, run this to start the deepstream app"
printf "\n       deepstream-app -c yolo_kafka/ds_configs/deepstream_app_config.txt \n\n"
printf "\n       GST_DEBUG_DUMP_DOT_DIR=./logs deepstream-app -c yolo_kafka/ds_configs/deepstream_app_config.txt \n\n"
echo "${NAME} Must run this if you are running the command through ssh: export DISPLAY=:0 "