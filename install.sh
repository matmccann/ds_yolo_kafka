#!/bin/bash

# Example usage: sudo -H ./install.sh
export NAME="[install.sh]: "
export HOME_DIR=$(pwd)

set -eE -o functrace
failure() {
  local lineno=$1
  local msg=$2
  echo "${NAME} Failed at $lineno: $msg"
}
trap '${NAME} failure ${LINENO} "$BASH_COMMAND"' ERR


echo "${NAME} STARTING SCRIPT"
export CUDA_VER=$1
echo "Jetson Setup for fresh install using cuda version ${CUDA_VER}."
echo "For more information about cuda version: nvcc --version "

echo "--(test)-- CUDA installed?"
if /usr/local/cuda/bin/nvcc --version; then
  echo "--(pass)-- continue with installation"
else
  echo "--(fail)-- cuda is not installed:  /usr/local/cuda/bin/nvcc --version"
  exit 1
fi

CUDA_INSTALLED="$(/usr/local/cuda/bin/nvcc --version | grep release | cut -d' ' -f 5 | cut -d',' -f1)"

if [[ "$CUDA_INSTALLED" == "$CUDA_VER" ]]; then
  echo "--(pass)-- Cuda version is ${CUDA_VER} as expected"
else
  echo "--(fail)-- Incorrect cuda version installed. Check your version: /usr/local/cuda/bin/nvcc --version | grep release"
  echo "--(msg)-- Then try this: sudo -H ./install.sh <version> "
  echo "--(msg)-- Example: sudo -H ./install.sh '10.2' "
  exit 1
fi

echo "${NAME} Copy kafka libraries"
echo "--(test)-- kafka libs exist"
if ls /opt/nvidia/deepstream/deepstream/lib/librdkafka.so; then
  echo "--(pass)-- No need ot copy kafka lib files, they are already at /opt/nvidia/deepstream/deepstream/lib"
else
  if ls /usr/local/lib/librdkafka.so; then
    echo "--(task)-- Coping files that are on hardware ..."
    cp -r /usr/local/lib/librdkafka* /opt/nvidia/deepstream/deepstream/lib/ --;
  else
    echo "--(task)-- Coping files from our directory ..."
    cd $HOME_DIR/kafka_libs; echo "Current Directory: `pwd`"; cp -r *kafka* /usr/local/lib/ --;
    cp *kafka* /opt/nvidia/deepstream/deepstream/lib/ --;
  fi
  ldconfig
fi

echo "${NAME} Install apt libraries"
apt-get install -y libglib2.0 libglib2.0-dev libjansson4  libjansson-dev
echo "${NAME} Set up deepstream plugins and base app"
cd /opt/nvidia/deepstream/deepstream/sources/libs/nvmsgconv/;
make --;
make install --;
cd /opt/nvidia/deepstream/deepstream/sources/apps/sample_apps/deepstream-app;
make --;
make install --;

echo "${NAME} Build nvdsinfer_custom_impl_Yolo"
cd $HOME_DIR/yolo_kafka/nvdsinfer_custom_impl_Yolo;
make clean --;
make --;
mkdir -p $HOME_DIR/logs
echo "sudo apt-get install -y graphviz" >> $HOME_DIR/logs/readme.txt
echo "dot -Tpng file.dot > file.png" >> $HOME_DIR/logs/readme.txt
chmod +x $HOME_DIR -R;

echo "${NAME} Install base Gstreamer libraries"
apt-get install -y \
    build-essential graphviz tzdata python3-pip git pkg-config apt-utils curl \
    libssl1.0.0 \
    libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev \
    gstreamer1.0-plugins-base \
    gstreamer1.0-doc \
    gstreamer1.0-x \
    gstreamer1.0-gl \
    gstreamer1.0-gtk3 \
    gstreamer1.0-qt5 \
    gstreamer1.0-pulseaudio \
    gstreamer1.0-tools \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly \
    gstreamer1.0-libav \
    gstreamer1.0-alsa \
    libgstrtspserver-1.0-0 \
    libopencv-dev \
    libglib2.0-0 libglib2.0-bin libglib2.0-dev-bin \
    libglib2.0-dev libjson-glib-dev libglib2.0 \
    uuid-dev \
    libjansson4  libjansson-dev libssl-dev json-glib-1.0 \
    libhdf5-dev graphviz \
    libgirepository1.0-dev \
    libcairo2-dev python3-dev \
    python-gi-dev python3-gi python3-gst-1.0 \
    python3-confluent-kafka

echo "${NAME} Clean up."
apt-get update -y
apt-get autoclean

echo "${NAME} FINISHED."
echo "Run this: sudo ./run.sh"
