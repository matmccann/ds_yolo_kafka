/**
 * librdkafka - Apache Kafka C library
 *
 * Copyright (c) 2012, Magnus Edenhill
 * All rights reserved.
 * https://github.com/edenhill/librdkafka
 *
 *
 * gcc -Wall producer.c -o producer `pkg-config --cflags --libs glib-2.0` `pkg-config --cflags --libs rdkafka`
 * ./consumer '172.22.0.3:9092' 'DS' 'test'
 * ./consumer
 *
 */
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <librdkafka/rdkafka.h>

#define KAFKA_POLL_WAIT 1000 // block thread and wait for 1000ms to poll
#define KAFKA_POLL_NOW 0 // immediately poll for a new message
#define KAFKA_POLL_FLUSH 10000 // wait for 10s (10,000ms), poll, and flush all messages
static int run = 1;

/**
 * @brief Signal termination of program
 */
static void stop (int sig) {
    run = 0;
    fclose(stdin); /* abort fgets() */
}


/**
 * @brief Message delivery report callback.
 *
 * This callback is called exactly once per message, indicating success or failure
 * The callback is triggered from rd_kafka_poll() and executes on the application's thread.
 */
static void dr_msg_cb (rd_kafka_t *rk, const rd_kafka_message_t *rkmessage, void *opaque) {
    if (rkmessage->err)
        fprintf(stderr, "%% Message delivery failed: %s\n",
                rd_kafka_err2str(rkmessage->err));
    else
        fprintf(stderr,
                "%% Message delivered (%zd bytes, "
                "partition %"PRId32")\n",
            rkmessage->len, rkmessage->partition);

    /* The rkmessage is destroyed automatically by librdkafka */
}


int main (int argc, char **argv) {
    rd_kafka_t *producer;
    rd_kafka_topic_t *producer_topic;
    rd_kafka_conf_t *producer_config;
    char error_buffer[512];       /* librdkafka API error reporting buffer */
    char kafka_payload[512];          /* Message value temporary buffer */

    /* Argument validation */
    if (argc != 3) {
        fprintf(stderr, "%% Usage: %s <broker> <topic>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char *brokers = argv[1];
    const char *topic   = argv[2];

    /* Create Kafka client configuration place-holder */
    producer_config = rd_kafka_conf_new();

    /**
     * Set bootstrap broker(s) as a comma-separated list to set brokers from the cluster.
     * @params: host:port,host:port,host:port
     */
    if (rd_kafka_conf_set(producer_config, "bootstrap.servers", brokers, error_buffer, sizeof(error_buffer)) != RD_KAFKA_CONF_OK) {
        fprintf(stderr, "%s\n", error_buffer);
        return EXIT_FAILURE;
    }

    /* Set the delivery report callback (invoked per message) */
    rd_kafka_conf_set_dr_msg_cb(producer_config, dr_msg_cb);

    /* create producer instance */
    producer = rd_kafka_new(RD_KAFKA_PRODUCER, producer_config, error_buffer, sizeof(error_buffer));
    if (!producer) {
        fprintf(stderr, "%% Failed to create new producer: %s\n", error_buffer);
        return EXIT_FAILURE;
    }

    /* Create topic object that will be reused for each message produced */
    producer_topic = rd_kafka_topic_new(producer, topic, NULL);
    if (!producer_topic) {
        fprintf(stderr, "%% Failed to create topic object: %s\n", rd_kafka_err2str(rd_kafka_last_error()));
        rd_kafka_destroy(producer);
        return EXIT_FAILURE;
    }

    /* Signal handler for clean shutdown */
    signal(SIGINT, stop);

    fprintf(stderr,
            "%% Type some text and hit enter to produce message\n"
            "%% Or just hit enter to only serve delivery reports\n"
            "%% Press Ctrl-C or Ctrl-D to exit\n");

    while (run && fgets(kafka_payload, sizeof(kafka_payload), stdin)) {
        size_t len = strlen(kafka_payload);

        if (kafka_payload[len-1] == '\n') /* Remove newline */
            kafka_payload[--len] = '\0';

        if (len == 0) {
            /* Empty line: only serve delivery reports */
            rd_kafka_poll(producer, 0/*non-blocking */);
            continue;
        }

        /*
         * Asynchronous call to send/produce message;  Background threads calls on
         * The actual delivery attempts to the broker are handled by background thread cb: rd_kafka_conf_set_dr_msg_cb().
         */
        retry:
        if (rd_kafka_produce(
                producer_topic, RD_KAFKA_PARTITION_UA, RD_KAFKA_MSG_F_COPY, kafka_payload, len, NULL, 0, NULL) == -1) {
            /* display error message */
            fprintf(stderr, "%% Failed to produce to topic %s: %s\n",
                    rd_kafka_topic_name(producer_topic), rd_kafka_err2str(rd_kafka_last_error()));

            /* Poll to handle delivery reports */
            if (rd_kafka_last_error() ==
                RD_KAFKA_RESP_ERR__QUEUE_FULL) {
                /* If the internal queue is full, block the thread - wait for message delivery - and then retry. */
                rd_kafka_poll(producer, KAFKA_POLL_WAIT);
                goto retry;
            }
        } else {
            fprintf(stderr, "%% Enqueued message (%zd bytes) for topic %s\n", len, rd_kafka_topic_name(producer_topic));
        }

        /* Must call it after every rd_kafka_produce() call */
        rd_kafka_poll(producer, KAFKA_POLL_NOW);
    }

    /* Wait for final messages to be delivered or fail. */
    fprintf(stderr, "%% Flushing final messages..\n");
    rd_kafka_flush(producer, KAFKA_POLL_FLUSH /* wait for max 10 seconds */);

    /* Destroy topic object */
    rd_kafka_topic_destroy(producer_topic);

    /* Destroy the producer instance */
    rd_kafka_destroy(producer);

    return EXIT_SUCCESS;
}
