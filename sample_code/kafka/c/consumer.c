/*
 * librdkafka - Apache Kafka C library
 *
 * Copyright (c) 2012, Magnus Edenhill
 * All rights reserved.
 * https://github.com/edenhill/librdkafka
 *
 *
 * gcc -Wall consumer.c -o consumer `pkg-config --cflags --libs glib-2.0` `pkg-config --cflags --libs rdkafka` -lpthread
 * ./consumer '172.22.0.3:9092' 'DS' 'test'
 * ./consumer
 *
 */

#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include <glib.h>
#include <librdkafka/rdkafka.h>
#include <pthread.h>
//#include "libs/json.h"

static volatile sig_atomic_t run = 1;
const char *EMPTY_STRING = "\0";

/* kafka arguments */
struct KafkaArgs {
    int argc;
    char **argv;
} KafkaArgs;

static void *kafka_connect(void *args) {
    struct KafkaArgs *params = (struct KafkaArgs *) args;

    rd_kafka_t *rk;           // Consumer instance handle
    rd_kafka_conf_t *conf;    // Temporary configuration object
    rd_kafka_resp_err_t err;  // librdkafka API error code
    char errstr[512];         // librdkafka API error reporting buffer
    const char *brokers = params->argv[1];
    const char *groupid = params->argv[2];
    const char *topic = params->argv[3];
    int num_topics = 1;
    rd_kafka_topic_partition_list_t *partition;  // Subscribed topics
    conf = rd_kafka_conf_new();

    if (rd_kafka_conf_set(conf, "bootstrap.servers", brokers, errstr,
                          sizeof(errstr)) != RD_KAFKA_CONF_OK) {
        fprintf(stderr, "%s \n", errstr);
        rd_kafka_conf_destroy(conf);
        return NULL;
    }
    g_print("--(config)-- kafka bootstrap.servers (broker): %s \n", brokers);

    if (rd_kafka_conf_set(conf, "group.id", groupid, errstr, sizeof(errstr)) !=
        RD_KAFKA_CONF_OK) {
        fprintf(stderr, "%s \n", errstr);
        rd_kafka_conf_destroy(conf);
        return NULL;
    }
    g_print("--(config)-- kafka group.id: %s \n", groupid);


    rk = rd_kafka_new(RD_KAFKA_CONSUMER, conf, errstr, sizeof(errstr));
    if (!rk) {
        fprintf(stderr, "%% Failed to create new consumer: %s \n", errstr);
        return NULL;
    }

    conf = NULL;
    rd_kafka_poll_set_consumer(rk);
    partition = rd_kafka_topic_partition_list_new(0);
    rd_kafka_topic_partition_list_add(partition, topic, RD_KAFKA_PARTITION_UA);

    /* Subscribe to the list of topics */
    err = rd_kafka_subscribe(rk, partition);
    if (err) {
        fprintf(stderr, "%% Failed to subscribe to %d topics: %s \n",
                partition->cnt, rd_kafka_err2str(err));
        rd_kafka_topic_partition_list_destroy(partition);
        rd_kafka_destroy(rk);
        return NULL;
    }
    g_print("--(config)-- kafka subscribed to topic: %s \n", topic);
    rd_kafka_topic_partition_list_destroy(partition);

    /* poll for kafka messages */
    while (run) {
        rd_kafka_message_t *rkm;

        rkm = rd_kafka_consumer_poll(rk, 100);
        if (!rkm) continue;  // Timeout: no message within 100ms,

        if (rkm->err) {
            fprintf(stderr, "%% Consumer error: %s \n", rd_kafka_message_errstr(rkm));
            rd_kafka_message_destroy(rkm);
            continue;
        }

        /* Print the message value/payload. */
        if (rkm->payload) {
            /* NOTE this data is not a string ( it consists of all string elements produced by kafka ) */
            g_print("--(consumer)-- payload received \n");
            g_print("%s \n", (const char *) rkm->payload);
        }
        rd_kafka_message_destroy(rkm);
    }

    /* Close the consumer: commit final offsets and leave the group. */
    fprintf(stderr, "%% Closing consumer\n");
    rd_kafka_consumer_close(rk);
    rd_kafka_destroy(rk);

    return NULL;
}


int main(int argc, char **argv) {
    if (argc != 4) {
        g_printerr("-- Invalid args \n");
        g_print("-- Try this: ./consumer '172.22.0.3:9092' 'seamist' 'test' \n");
        return EXIT_FAILURE;
    }

    g_print("--(run)-- starting kafka consumer \n");
    printf("--(run)-- run state: %d \n", run);
    struct KafkaArgs kafkaParams;
    kafkaParams.argv = argv;
    kafkaParams.argc = argc;
    pthread_t kafka_thread;
    if (pthread_create(&kafka_thread, NULL, kafka_connect, (void *) &kafkaParams)) {
        fprintf(stderr, "Error creating thread\n");
        return 1;
    }
    else {
        g_print("--(run)-- kafka thread started \n");
    }

    /* close kafka thread */
    if (pthread_join(kafka_thread, NULL)) {
        fprintf(stderr, "Error joining thread\n");
        return 2;
    }
    g_print("--(run)-- kafka thread shutdown \n");

    /*
     *  struct KafkaArgs params = {.argc = 1, .broker = broker, .groupid = groupid, .topic = topic};
     *  g_print("--(config)-- args:  (%d, %s, %s, %s) \n", params.argc, params.broker, params.groupid, params.topic);
     */
    printf("--(run)-- Exiting successfully \n");
    return EXIT_SUCCESS;
}
