#!/usr/bin/env python3

# /kafka/kafka_driver/run_producer.py
from __future__ import absolute_import
import logging

from kafka.kafka_driver.kafka_setup.producer import ProducerClass
from kafka.kafka_driver.kafka_setup.utils import setup_logging
import json


LOGGING = "<run_producer.py> :: "


if __name__ == "__main__":

    """
    An example of using ProducerClass
    """
    import sys
    from time import sleep
    setup_logging()

    if len(sys.argv) != 2:
        print(f"{LOGGING}Usage: python3 {sys.argv[0]} <kafka-topic>")
        exit()

    logging.info(f"{LOGGING} An example of using ProducerClass")
    logging.info(f"Open kafkacat with this:  sudo docker exec -it kafkacat kafkacat -b 172.22.0.3:9092 -t {sys.argv[1]} -C")
    sleep(5)

    topic = sys.argv[1]

    producer_config = "/kafka/kafka_driver/kafka_setup/config/kafka/producer/ds_seamist.json"
    logging.info(f"starting producer (topic = {topic}; avro_schema = False)")
    my_producer = ProducerClass(config_file=producer_config, schema_file=None)

    file_path = "/kafka/ds_output/"

    num_frames = 1
    sensors = [0,1,2,3]
    frame = -1
    while frame < num_frames-1:
        frame +=1
        for ir_camera in sensors:
            json_file = f"sensor{ir_camera}_{frame}.json"
            json_path = file_path + json_file
            with open(json_path,'r') as f:
                data = json.load(f)
                print(f"Sending payload: {json_file}")
                my_producer.send_dict_message(data)
            sleep(0.4)

    logging.info("Finished the example.")
