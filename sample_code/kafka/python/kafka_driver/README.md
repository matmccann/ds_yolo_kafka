![python](https://img.shields.io/badge/python-3.9.2-green)
#> Kafka Cassandra Driver

- Configurable kafka topic script that writes to cassandra with its appropriate database model


------
------

# Quick Start
- use case example
- note that you must mount directory under docker-compose.yml 
- don't forget to `python3 -m pip install -r requirements.txt` for kafka dependencies
```bash
sudo docker exec -it database bash -c "python3 kafka/kafka_driver/run_consumer.py nvidia False"
sudo docker exec -it fusion bash -c "python3 kafka/kafka_driver/run_consumer.py localizer True"
sudo docker exec -it database bash -c "python3 kafka/kafka_driver/run_consumer.py localizer False"
```

__using the producer to do a data dump__

- setup the consumer with kafkacat
```bash
sudo make kafkacat-infer
sudo make infer
```
- run the producer to dump sailboat data
```bash
sudo make kafkacat-infer
```

- here are the available data dumps setup in the Makefile
```
    sudo docker-compose -f docker-compose-panda.yml up -d --build <insert-from-below>
    kafka_infer
    kafka_ais    
    kafka_track
```
