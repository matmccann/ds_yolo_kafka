# /kafka/kafka_driver/__init__.py

import sys
import os

from . import kafka_setup  # noqa: F401

sys.path.append(os.path.abspath(os.path.join(os.path.dirname(__file__), "../")))
