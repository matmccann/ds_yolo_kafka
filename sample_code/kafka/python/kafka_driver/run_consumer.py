#!/usr/bin/env python3

# /kafka/kafka_driver/run_consumer.py
from __future__ import absolute_import
import json
import logging
import sys
import time
from kafka.kafka_driver.kafka_setup.consumer import ConsumerClass
from kafka.kafka_driver.kafka_setup.utils import setup_logging
from os import system
from pathlib import Path

LOGGING = "<run_consumer.py>\t| "


def save_to_json(data, file_path):
    Path(file_path).touch(mode=0o777, exist_ok=False)

    try:
        with open(file_path, 'w') as outfile:
            json.dump(data, outfile)
    except Exception as json_write_error:
        logging.error(f"{LOGGING} Did not save {file_path}\n")
        raise json_write_error
    return


def consumer_streamer(kafka, validate=False):  # noqa 501
    # tracks the frame number you've received
    counter = -1
    # set up data dump output
    storage_folder = "/jetson_media/ds_output/"
    system(f"rm -rf {storage_folder}")
    system(f"mkdir -p {storage_folder}")

    # set to run the loop indefinitely until you receive a msg='bang'
    kafka.enter()
    kafka.active = True
    logging.debug(f"{LOGGING} Consumer is waiting for messages now ... \n*****\n")
    while kafka.active is True:

        try:
            msg = kafka.poll(timeout=1e-3, schema_validation=validate)
            # logging.info(f"{LOGGING}{time.asctime(time.localtime())} Poll msg ...")

            if not msg:
                continue

            # expects values of dictionary to match
            if msg == 'bang':
                logging.info(f"{LOGGING}{time.asctime(time.localtime())}\tRCVD bang: terminating kafka consumer")
                kafka.active = False
                break
            data = None
            try:
                data = json.loads(msg)

                #
                # Add your payload deconstruction here and modify `data`
                #

                counter += 1
                logging.info(f"{LOGGING}{time.asctime(time.localtime())}\t RCVD msg({counter})")
                # logging.debug(f"{LOGGING}{time.asctime(time.localtime())}\t RCVD msg({counter})\nMSG:{msg}")
                file_name = f"sensor{data['sensor']}_{data['frame_number']}.json"
                try:
                    save_to_json(data=data, file_path=storage_folder+f"{file_name}")
                except Exception as err:
                    logging.error(f"{LOGGING} ERROR save_to_json(): {err}")

            except Exception as err:
                logging.error(f"{LOGGING}{time.asctime(time.localtime())}\t ERROR: {err}\nKAFKA_MSG:{msg}")


        except Exception as err:
            print("**********")
            print(err)
            kafka.active = False
    # Close kafka consumer
    kafka.exit()


if __name__ == "__main__":
    """
    An example of using ConsumerClass
    """
    from time import sleep
    if len(sys.argv) != 2:
        print(f"Example usage:\tpython3 {sys.argv[0]} <topic>")
        print(f"Try this:\tpython3 {sys.argv[0]} 'ir'")
        exit()

    topic = sys.argv[1]

    # Configure module wide logging
    setup_logging()
    logging.info(f"{LOGGING} kill your stream with this: bang\n")
    logging.info(f"{LOGGING} start a producer: sudo docker exec -it kafkacat kafkacat -b 172.22.0.3:9092 -t '{topic}' -P")
    sleep(5)

    c_config = "/kafka/kafka_driver/kafka_setup/config/kafka/consumer/seamist_ir.json"
    my_consumer = ConsumerClass(config_file=c_config, schema_file=None)
    consumer_streamer(kafka=my_consumer, validate=False)
    print("Finished example")
