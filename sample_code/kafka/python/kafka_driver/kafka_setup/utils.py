# /kafka/kafka_driver/kafka_setup/config/helpers.py

import io
import json
import avro.schema
import fastavro
import logging
import socket

LOGGING = "<kafka_setup/utils.py> | "


from pathlib import Path
from os import system
import logging
from datetime import datetime


def setup_logging(folder="/logs/python/", filename="python.log", level=logging.DEBUG):
    try:
        system(f"mkdir -p {folder}")
        filepath = folder + filename
        Path(filepath).touch(mode=0o777, exist_ok=True)
        system(f"ln -sf /dev/stdout {filepath}")
        logging.basicConfig(filename=filepath, filemode="w", level=level)
        logging.debug(f"\t\tLOG FILE LOCATION: {filepath}")
        print(f"Python logger started at {datetime.now()}\n", flush=True)
        pass
    except Exception as err:
        print(f"ERROR (setup_logging): {err}")


def load_json_file(input_file):
    """Load json applicationData from a file and return list of json objects.

    Parameters
    ----------
    input_file : `string`
        File path to file.json or file.txt

    Returns
    -------
    `list`
        list of dicts for each line or json object in input_file.
    """
    json_object_list = []

    with open(input_file, "r") as f:
        for json_line in f:
            object_dict = json.loads(json_line)
            json_object_list.append(object_dict)

    return json_object_list


def loadSingleAvsc(file_path, schema_names=avro.schema.Names()):
    """Load avro schema from a single file 'schema.avsc'.

    Parameters
    ----------
    file_path : `string`
        File path to schema.avsc.
    schema_names : `object`
        Object containing permissible avro schema names.

    Returns
    -------
    `json`
        json format of avro schema.
    """
    with open(file_path) as file_text:
        json_data = json.load(file_text)
    schema = avro.schema.SchemaFromJSONData(json_data, schema_names)
    return schema.to_json()


def writeAvroData(json_data, json_schema):
    """Encode json into Avro format given a schema.

    Parameters
    ----------
    json_data : `dict`
        The JSON applicationData containing message content.
    json_schema : `dict`
        The writer Avro schema for encoding applicationData.

    Returns
    -------
    `_io.BytesIO`
        Encoded applicationData.
    """
    bytes_io = io.BytesIO()
    fastavro.schemaless_writer(bytes_io, json_schema, json_data)
    return bytes_io


def readAvroData(bytes_io, json_schema):
    """Read applicationData and decode with a given Avro schema.

    Parameters
    ----------
    bytes_io : `_io.BytesIO`
        Data to be decoded.
    json_schema : `dict`
        The reader Avro schema for decoding applicationData.

    Returns
    -------
    `dict`
        Decoded applicationData.
    """
    bytes_io.seek(0)
    message = fastavro.schemaless_reader(bytes_io, json_schema)
    return message


def load_schema(schema_files, schema_names=avro.schema.Names()):
    """Load avro schema.

    Parameters
    ----------
    schema_files : `string`
        File path to schema.avsc.
    schema_names : `object`
        Object containing permissible avro schema names.

    Returns
    -------
    `json`
        json format of avro schema.
    """
    if schema_files is not None:
        return loadSingleAvsc(schema_files, schema_names)
    else:
        return None


def load_config(config_file, consumer_model=True):
    """Load configuration for Consumer or Producer from file

    Parameters
    ----------
    config_file : `string`
        File path to config_file.json.
    consumer_model : `boolean`
        Control variable for loading consumer or producer models.

    Returns
    -------
    `config`
        Configurations to be passed into Consumer instantiation.
    """
    config = None

    if config_file is not None:
        try:
            logging.info(f"{LOGGING} Loading kafka configuration file: {config_file}")
            with open(config_file) as configurations:
                if consumer_model is True:
                    config = json.load(configurations)
                    logging.info(f"{LOGGING} Loaded config file for kafka consumer")
                else:
                    config, topic = producer_configs(configurations)

        except IOError:
            logging.error(f"{LOGGING} File not found\t\t\t|  {config_file}")
        except ValueError:
            logging.error(f"{LOGGING} File has invalid json\t\t|  {config_file}")
    else:
        raise Exception("Configuration file not passed on instantiation. ")
    return config



def producer_configs(configurations):
    """Organize producer configs for def load_config()

    Parameters
    ----------
    configurations : `textIO`
        File handler for path/to/config.json.

    Returns
    -------
    `configs, topic`
        Bootstrap configs and topic Producer instantiation.
    """
    configs = json.load(configurations)
    config = configs["bootstrap"]
    config["client.id"] = socket.gethostname()
    topic = configs["topic"]
    return configs, topic


def get_consumer_topic_configurations(file_path):

    if 'seamist' in topic:
        config = f"/kafka/kafka_driver/kafka_setup/config/kafka/consumer/{topic}.json"
    else:
        msg = f"Incorrect configuration passed: {topic}"
        logging.error(msg)
        raise Exception(msg)
    return config
