# /kafka/kafka_driver/kafka_setup/config/consumer.py
import logging
import time
import io
# import json
from confluent_kafka import Consumer, KafkaError
from kafka.kafka_driver.kafka_setup import utils
from fastavro.validation import validate

LOGGING = "<consumer.py>\t| "


class ConsumerClass:
    def __init__(self, config_file=None, schema_file=None):
        """Kafka consumer

        Parameters
        ----------
        config_file : `string`
            File path to schema.json
        schema_file : `string` or `None`
            File path to schema.avsc
        """
        self.schema = utils.load_schema(schema_file)
        self.config = utils.load_config(config_file, consumer_model=True)
        self.consumer = None
        self.topic = self.config['topic']
        self.active = True
        logging.info(f"{LOGGING} Consumer started:\t\t\t| config: {self.config}\tschema: {self.schema}")

    def enter(self):
        """Instantiate consumer and start subscription"""
        self.consumer = Consumer(self.config["bootstrap"])
        self.consumer.subscribe([self.config["topic"]])

    def exit(self):
        """Clear instantiation and close consumer"""
        self.consumer.close()

    def __enter__(self):
        """Instantiate consumer for context manager"""
        self.consumer = Consumer(self.config["bootstrap"])
        self.consumer.subscribe([self.config["topic"]])
        return self

    def __exit__(self):
        """Clear instantiation and close consumer from context manager"""
        self.consumer.close()

    def poll(self, timeout=1e-3, schema_validation=False):
        """Continuous streaming the kafka topic.

        Parameters
        ----------
        timeout : `int`
            File path to schema.json
        schema_validation : `bool`
            File path to schema.avsc

        Returns
        -------
        'json' or `dict`
            Returns 'json' if not schema_validation else returns 'dict'
        """
        msg = None
        try:
            msg = self.consumer.poll(timeout)

            if msg is None:
                return None
            if msg.error():
                err_msg = msg.error()
                if err_msg.name() == '_PARTITION_EOF':
                    logging.info(f"{LOGGING} Reached end of topic {self.topic} {msg.partition()} at offset {msg.offset()}")
                else:
                    logging.error(f"{LOGGING} Consumer Error \t | {msg.error()}")
                    raise PartitionEndError(msg)
            return self.decodeMSG(msg, schema_validation)

        except Exception as err:
            raise Exception(f"{LOGGING} Unknown error\t | (err): {err}\t\t(msg) {msg}")

    def decodeMSG(self, msg, schema_validation=False):
        """Decode Avro message according to a schema.
        Parameters
        ----------
        msg : Kafka message
            The Kafka message result from consumer.poll().
        schema_validation: use deserializer to decode/validate applicationData
        Returns
        -------
        `json` or `dict`
            Decoded message: `json` if not schema_validation, else `dict`.
        """
        decoded_msg = None

        if schema_validation is False:
            # logging.debug(f"{LOGGING} ConsumerClass.decodeMSG\t\t| msg received")
            return msg.value().decode("utf-8")
        else:
            try:
                message = msg.value()
                bytes_io = io.BytesIO(message)
                decoded_msg = utils.readAvroData(bytes_io, self.schema)
                validator = validate(decoded_msg, self.schema, raise_errors=False)
                if validator is True:
                    return decoded_msg
                else:
                    return None
            except AssertionError as err:
                logging.error(f"{LOGGING} Consumer.decodeMSG\t\t| AssertionError: err = {err}")
            except IndexError as err:
                logging.error(f"{LOGGING} Consumer.decodeMSG\t\t| IndexError: err = {err}")
            except Exception as err:
                logging.error(f"{LOGGING} Consumer.decodeMSG\t\t| Exception: (check schema format):: err = {err}")

        return decoded_msg


class AlertError(Exception):
    """Base class for exceptions in this module."""
    pass


class PartitionEndError(AlertError):
    """Exception raised when reaching end of partition.
    Parameters
    ----------
    msg : Kafka message
        The Kafka message result from consumer.poll().
    """

    def __init__(self, msg):
        message = (
            "topic:%s, partition:%d, status:end, "
            "offset:%d, key:%s, time:%.3f\n"
            % (msg.topic(), msg.partition(), msg.offset(), str(msg.key()), time.time())
        )
        self.message = message
        logging.error(f"{LOGGING} PartitionEndError\t\t\t| partition error: {message}")

    def __str__(self):
        return self.message
