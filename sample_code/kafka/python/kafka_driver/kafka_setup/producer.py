# /kafka/kafka_driver/kafka_setup/config/producer.py

import logging
import time
from confluent_kafka import Producer
from fastavro._validate_common import ValidationError
import json
from kafka.kafka_driver.kafka_setup.constants import the_sentinel
from kafka.kafka_driver.kafka_setup import utils

LOGGING = "\t<producer.py>\t\t| "


class ProducerClass:
    def __init__(self, config_file=None, schema_file=None):
        """Kafka producer

        Parameters
        ----------
        config_file : `string`
            File path to schema.json
        schema_file : `string` or `None`

        """
        self.config = utils.load_config(config_file, consumer_model=False)
        assert self.config is not None

        if schema_file is None:
            self.schema = None
        else:
            self.schema = utils.load_schema(schema_file)
        self.topic = self.config["topic"]
        self.producer = Producer(self.config["bootstrap"])
        self.active = True
        self.data = "Kafka producer initialized"

        # Log instantiation of a producer
        logging.info(f"{LOGGING} Producer started:\t\t\t| config\t{self.config}\tschema\t{self.schema}")

    def send(self, send_data, schema_encode=True):
        """Kafka Producer send applicationData

        Parameters
        ----------
        send_data : `dict`
            Dictionary of applicationData to send
        schema_encode : `boolean`
            logic variable for encoding or not

        """
        self.data = send_data

        if schema_encode is True:
            my_bytes = utils.writeAvroData(self.data, self.schema)
            raw_bytes = my_bytes.getvalue()
            self.producer.produce(self.topic, raw_bytes, callback=self.delivery_report)
            self.producer.flush()
        else:
            self.producer.produce(self.topic, self.data, callback=self.delivery_report)
            self.producer.flush()

    def delivery_report(self, err, msg):
        """Asynchronous callback for producer.produce() that logs results.
        Triggered by poll() or flush().

        Parameters
        ----------
        err : `string`
            Error type
        msg : `string`
            Callback message

        """
        if err is not None:
            logging.warning("Message delivery failed: {}".format(err))
        else:
            pass
            # message = 'Message {} delivered to {} [{}]'
            # logging.debug(message.format(self.data,msg.topic(),msg.partition()))

    def send_dict_message(self, payload):
        """Asynchronous send.
        Wait up to poll(n) second for events.
        Callbacks invoked if the message is acknowledged..

        Parameters
        ----------
        payload : `dict`
            kafka payload to be sent to message broker.

        """
        log_msg = f"Sending kafka payload:: {payload}"
        logging.debug(log_msg)
        self.data = payload
        self.producer.produce(self.topic, json.dumps(self.data), callback=self.delivery_report)

    def producer_write(self, msg, encode=False):
        """

        Parameters
        ----------
        msg: message to be sent into messaging fabric
        encode: option to send json (encode=False) or use avro serialization schema

        """
        logging.debug(f"{LOGGING} entered producer_write")

        if self.active is True:

            try:
                if msg == the_sentinel:
                    logging.info(
                        f"{LOGGING}\t"
                        f"{time.asctime(time.localtime())}\t"
                        f"Kafka Sentinel SENT: terminating kafka producer."
                    )
                    self.active = False
                    self.send(send_data=msg, schema_encode=encode)
                try:
                    logging.debug(f"{LOGGING}\t{time.asctime(time.localtime())}\tPAYLOAD:\n{msg}")
                    self.send(send_data=msg, schema_encode=encode)

                except ValidationError as err:
                    logging.error(f"{LOGGING}\t{time.asctime(time.localtime())}\tERROR: {err}\tPAYLOAD:\n{msg}")
                except Exception as err:
                    logging.error(f"{LOGGING}Killing producer_write() due to ERROR: \n{err}.")
                    logging.error(f"{LOGGING}\t{time.asctime(time.localtime())}\tPAYLOAD:\n{msg}\n")
                    self.active = False

            except KeyboardInterrupt:
                # return false if incomplete
                self.active = False
                return
        else:
            raise Exception("No active kafka Producer; Try making this change ==> producer.active=True")
        return
