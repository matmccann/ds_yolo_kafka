
#include <gst/gst.h>
#include <glib.h>

#include <stdbool.h>
#include <stdlib.h>

/* If running on Jetson change to 1 */
#define JETSON 1


typedef struct _TsDemuxLinks {
    GstElement *video;
    GstElement *klv;
} TsDemuxLinks;

/* Translates link_pad() error to a readable error */
gchar *get_link_error(int error_code) {
    gchar *ret_str = "ERROR: get_link_error() doesn't recognize error_code";
    switch (error_code) {
        case 0:
            ret_str = "GST_PAD_LINK_OK (0) – link succeeded";
            break;
        case -1:
            ret_str = "GST_PAD_LINK_WRONG_HIERARCHY (-1) – pads have no common grandparent";
            break;
        case -2:
            ret_str = "GST_PAD_LINK_WAS_LINKED (-2) – pad was already linked";
            break;
        case -3:
            ret_str = "GST_PAD_LINK_WRONG_DIRECTION (-3) – pads have wrong direction";
            break;
        case -4:
            ret_str = "GST_PAD_LINK_NOFORMAT (-4) – pads do not have common format";
            break;
        case -5:
            ret_str = "GST_PAD_LINK_NOSCHED (-5) – pads cannot cooperate in scheduling";
            break;
        case -6:
            ret_str = "GST_PAD_LINK_REFUSED (-6) – refused for some reason";
            break;
    }
    return ret_str;
}

/* manage bus callbacks from the pipeline */
static gboolean bus_call(GstBus *bus, GstMessage *msg, gpointer data) {
    GMainLoop *loop = (GMainLoop *) data;

    switch (GST_MESSAGE_TYPE(msg)) {

        case GST_MESSAGE_EOS:
            g_print("End of stream\n");
            g_main_loop_quit(loop);
            break;

        case GST_MESSAGE_ERROR: {
            gchar *debug;
            GError *error;

            gst_message_parse_error(msg, &error, &debug);
            g_free(debug);

            g_printerr("Error: %s\n", error->message);
            g_error_free(error);

            g_main_loop_quit(loop);
            break;
        }
        default:
            break;
    }

    return TRUE;
}


 /*
  * Splits one mpegts into its streams. Links invoked tsDemux src pad (video/private)
  * to its desired sink element (i.e video_parser, klv_parser).
 */
static void split_streams(GstElement *element, GstPad *pad, TsDemuxLinks *sinks) {
     /* tsDemux callback function: handles invocation of its "Element Signal" "pad-added". */
     GstPadLinkReturn ret;
     GstPad *sinkpad;
     GstElement *decoder;
     gchar *src_name = gst_element_get_name(element);
     gchar *src_pad_name = gst_pad_get_name(pad);
     gchar *link_video = strstr(src_pad_name, "video");
     gchar *link_klv = strstr(src_pad_name, "private");
 //    g_print("--(link)-- parsed new pad for names:     video::%s,     private::%s  \n", link_video, link_klv);

     if (!link_video && !link_klv) {
         g_printerr("--(link)-- cannot detect `video` or `private` stream on src pad \n");
         return;
     }

     if (link_video) {
         g_print("--(link)-- detected video stream \n");
         decoder = (GstElement *) sinks->video;
     } else if (link_klv) {
         g_print("--(link)-- detected klv stream \n");
         decoder = (GstElement *) sinks->klv;
     } else {
         g_printerr("--(link)-- detected unknown stream! \n");
         g_printerr("--(link)-- Incorrect configs ... FAIL SILENTLY! \n");
         return;
     }
     sinkpad = gst_element_get_static_pad(decoder, "sink");
     gchar *sink_name = gst_element_get_name(decoder);
     gchar *sink_pad_name = gst_element_get_name(sinkpad);

     g_print("--(link)-- src (%s::%s); sink (%s::%s)  \n", src_name, src_pad_name, sink_name, sink_pad_name);

     /* If our converter is already linked, we have nothing to do here */
    if (gst_pad_is_linked(sinkpad)) {
        g_print("--(link)-- %s::%s is already linked \n", sink_name, sink_pad_name);
        gst_object_unref(sinkpad);
        return;
    }

    /* Link the stream */
    ret = gst_pad_link(pad, sinkpad);
    if (GST_PAD_LINK_FAILED(ret)) {
        gchar *ret_str = get_link_error(ret);
        g_printerr("--(link)-- link response: %s \n", ret_str);
        return;
    }
    gst_object_unref(sinkpad);
    g_print("--(link)-- SUCCESS \n");
    return;
}


int main(int argc, char *argv[]) {
    /*
     * Build: gcc -Wall play-mpegts.c -o play-mpegts $(pkg-config --cflags --libs gstreamer-1.0)
     */
    /* Check input arguments */
    if (argc != 2) {
        g_printerr("Usage: %s <mpegts filename>\n", argv[0]);
        g_print("mpegts (video+klv):  ./play-mpegts /jetson_media/videos/red_boat/TeraSense.Red.Boat_EO.ts \n");
        return EXIT_FAILURE;
    }

    GMainLoop *loop;
    GstElement *pipeline;
    GstBus *bus;
    guint bus_watch_id;
    gchar *video_file = argv[1];
    /* Initialisation */
    gst_init(&argc, &argv);
    loop = g_main_loop_new(NULL, FALSE);

    /* Create gstreamer elements */
    pipeline = gst_pipeline_new("video-player");
    if (!pipeline) {
        g_printerr("Pipeline could not be created: [pipeline]. Exiting.\n");
        return EXIT_FAILURE;
    }

    GstElement *source, *tsdemux, *codec_parser, *decoder, *sink, *klv_parser, *capsfilter, *klv_sink;
    source = gst_element_factory_make("filesrc", "file-source");
    g_object_set(G_OBJECT(source), "location", video_file, NULL);

    tsdemux = gst_element_factory_make("tsdemux", "mpegts-demux");
    g_object_set(G_OBJECT(tsdemux), "parse-private-sections", 1, NULL);
    codec_parser = gst_element_factory_make("h264parse", "x-h264");
    klv_parser = gst_element_factory_make("queue", "x-klv");
    capsfilter = gst_element_factory_make("capsfilter", "meta/x-klv");
    klv_sink = gst_element_factory_make("fakesink", "klv_sink");
    g_object_set(G_OBJECT(klv_sink), "dump", 1, NULL);

    if (!JETSON) {
        decoder = gst_element_factory_make("avdec_h264", "h264-decoder");
        sink = gst_element_factory_make("autovideosink", "video-output");
    } else {
        decoder = gst_element_factory_make("nvv4l2decoder", "gpu-decoder");
        sink = gst_element_factory_make("nvoverlaysink", "video-output");
    }

    if (!source || !tsdemux || !codec_parser
        || !klv_parser || !capsfilter || !klv_sink
        || !decoder || !sink) {
        g_printerr("An element could not be created. Exiting.\n");
        return EXIT_FAILURE;
    }

    /* Add elements to bin*/
    gst_bin_add_many(GST_BIN(pipeline), source, tsdemux, codec_parser, decoder, sink, NULL);
    gst_bin_add_many(GST_BIN(pipeline), klv_parser, capsfilter, klv_sink, NULL);
    /* Link elements to create directional data flow */
    gst_element_link(source, tsdemux);
    gst_element_link_many(codec_parser, decoder, sink, NULL);
    gst_element_link_many(klv_parser, capsfilter, klv_sink, NULL);

    TsDemuxLinks tsdemux_links = {codec_parser, klv_parser};
    g_print("--(config)-- tsdemux link video : %s \n", gst_pad_get_name(tsdemux_links.video));
    g_print("--(config)-- tsdemux link klv   : %s \n", gst_pad_get_name(tsdemux_links.klv));

    /* manage bus and signals */
    g_signal_connect(tsdemux, "pad-added", G_CALLBACK(split_streams), &tsdemux_links); // link dynamic pad
    bus = gst_pipeline_get_bus(GST_PIPELINE(pipeline));
    bus_watch_id = gst_bus_add_watch(bus, bus_call, loop);
    gst_object_unref(bus);

    /* Set the pipeline to "playing" state*/
    g_print("******** STARTING PIPELINE...\n");
    g_print("-- Now playing: %s\n", video_file);
    gst_element_set_state(pipeline, GST_STATE_PLAYING);

    /* Runs loop until completion */
    g_main_loop_run(loop);

    /* Out of the main loop, clean up nicely */
    g_print("-- Returned, stopping playback\n");
    gst_element_set_state(pipeline, GST_STATE_NULL);

    g_print("-- Deleting pipeline\n");
    gst_object_unref(GST_OBJECT(pipeline));
    g_source_remove(bus_watch_id);
    g_main_loop_unref(loop);
    g_print("-- SUCCESS: exit. \n");
    return EXIT_SUCCESS;
}

