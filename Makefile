
help:
	@echo "Add this to ~/.bashrc: xhost +"
	@echo "sudo reboot"
	@echo "... haha now you have to ssh back in again ..."
	@echo "Then do this:"
	@echo "export DISPLAY=:0"
	@echo "cd /home/nvidia/code/deepstream"
	@echo "sudo chmod +x * -R"

run_docker:
	docker network prune
	docker-compose up -d --build

run_app:
	deepstream-app -c yolo_kafka/ds_configs/deepstream_app_config.txt

run_kafkacat:
	docker exec -it kafkacat kafkacat -b '172.22.0.3:9092' -t 'ir' -C

run_producer:
	docker exec -it kafka_mock bash -c "python3 run_producer 'ir'"
run_consumer:
	docker exec -it kafka_mock bash -c "python3 run_consumer.py 'ir'"
