---

# Video Analytics Pipeline

---

## Simulation

- uses two videos: HalifaxTop.mp4 and HalifaxBottom.mp4 

__formatting videos__
- refer to `videos/README.md`


## Building libraries

__pre-flight check__

- if running through ssh, do this on the jetson (or put it into `~/.bashrc` then run `source ~/.bashrc`)
```bash
xhost + 
```

__installations__

- and through ssh do the following and follow terminal instructions
```bash
make help
/usr/local/cuda/bin/nvcc --version | grep release
sudo -H ./install.sh '10.2'
```

__build kafka library__

**terminal 1**
```bash
export CUDA_VER=10.2;
cd yolo_kafka/nvdsinfer_custom_impl_Yolo
make clean && make;
cd -;
cd yolo_kafka/mist_msgconv;
make clean && make;
cd -;
cd yolo_kafka/mist_kafka_adaptor;
make clean && make;
cd test;
make clean && make;
./test_kafka_proto_async
```

- now you can open up a consumer and receive the payload
**terminal 2**
```bash
sudo docker exec -it kafkacat kafkacat -b 172.22.0.3:9092 -t ir -G test -C 
```

__running deepstream's sample app__
- this is located at top level dir
```bash
make run_docker
export USE_NEW_NVSTREAMMUX=no
make run_app
```


# Run yolo_kafka

__build the project__

- we assume you've already executed `make run_docker` in top level dir

```bash
cd yolo_kafka;
make help
<follow terminal instructions>
```

- check your cuda version for this: `nvcc --version`
- replace `10.2` with your version
```bash
export CUDA_VER=10.2
make clean && make
make run_app SOURCES=2 KLV=0
```

# Sample Code

- the sample code folder has examples for how to play a video, and then create a NN pipeline that does the same thing, and also creating a kafka consumer
- each Makefile has instructions to follow to build and run ...
```bash
make help
```

---

# Troubleshooting

- kafka: fix import with `<json.h>`

```bash
sudo apt-get install -y libjsoncpp-dev 
sudo ln -s /usr/include/jsoncpp/json/ /usr/include/json
```

---
fin!
