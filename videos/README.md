---

Videos for development

---

# Converting videos

__mp4 format__

- The .avi files in question do not provide proper encoding, and will need the following command run on them to supply a more realistic input to the pipeline (thanks Matt)
- The resulting output will be h.264 encoded mp4 files.
```bash
ffmpeg -i HalifaxTop.avi -pix_fmt yuv420p -map 0:v:0 -c:v libx264 HalifaxTop.mp4
ffmpeg -i HalifaxBottom.avi -pix_fmt yuv420p -map 0:v:0 -c:v libx264 HalifaxBottom.mp4
```

__mpegts format__
- and if you want MPEG-TS format
```bash
  ffmpeg -i HalifaxTop.avi -pix_fmt yuv420p -map 0:v:0 -c:v libx264 -f mpegts HalifaxTop.ts
  ffmpeg -i HalifaxBottom.avi -pix_fmt yuv420p -map 0:v:0 -c:v libx264 -f mpegts HalifaxBottom.ts 
```
