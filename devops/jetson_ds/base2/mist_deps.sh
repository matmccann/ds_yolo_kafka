#!/bin/bash

# Set up mist dependencies
# Example usage: sudo -H ./mist_deps.sh
set -eE -o functrace
export NAME="[mist_deps.sh]"

failure() {
  local lineno=$1
  local msg=$2
  echo "${NAME} Failed at $lineno: $msg"
}
trap '${NAME} failure ${LINENO} "$BASH_COMMAND"' ERR

echo "${NAME}: STARTING SCRIPT"
echo "***** Mist dependencies for jetson xavier (agx or nx) with python 3.6.9 ***** "
echo "Preflight checks ..."
PYTHON_VERSION="$(python3 --version | cut -d' ' -f 2)"
EXPECTED_PYTHON_VERSION='3.6.9'
WORK_DIR?=/jetson_mist/mist

if [[ "$PYTHON_VERSION" == "$EXPECTED_PYTHON_VERSION" ]]; then
  echo "--(pass)-- Python3 version $EXPECTED_PYTHON_VERSION"
else
  echo "********"
  echo "--(fail)-- Expects $EXPECTED_PYTHON_VERSION but found $(python3 --version)."
  echo
  exit 1
fi

echo "${NAME} INSTALLATION STEP 2: klvdata (python)"
python3 -m pip install --user klvdata --

INSTALLATION_PATH="$(test -e /root/.local/lib/python3.6/site-packages/klvdata  && echo 'yes' || echo 'no')"
if [[ "$INSTALLATION_PATH" == "yes" ]]; then
  echo "--(pass)-- klvdata installed"
else
  echo "--(fail)-- klvdata failed to install."
  exit 1
fi

sed -i '116,117 s/^/#/' /root/.local/lib/python3.6/site-packages/klvdata/common.py --
sed -i '122,123 s/^/#/' /root/.local/lib/python3.6/site-packages/klvdata/common.py --

echo "${NAME} INSTALLATION STEP 3: mist python3 requirements.txt"
python3 -m pip install --user -r $WORK_DIR/requirements.txt --

echo "${NAME} INSTALLATION STEP 4: kafkacat "
apt-get -y autoclean && apt-get -y clean && apt-get -y autoremove
apt-get update -y
apt-get install -y kafkacat
/usr/bin/kafkacat -V --

echo "${NAME} INSTALLATION STEP 5: GDAL (python) "
apt-get update -y
apt-get install -y libssl-dev zlib1g-dev gcc g++ git software-properties-common ffmpeg libgdal-dev python3-gdal gdal-bin
# libgdal20
apt-get install -y wget

echo "${NAME} INSTALLATION STEP 6: Mist Alpha dependencies for arm64 (python) "
apt-get update -y
apt-get install -y build-essential libssl-dev libffi-dev libblas3 libc6 liblapack3 gcc python3-dev python3-pip cython3 python3-numpy
python3 -m pip install --user Cython==0.29.21 --
python3 -m pip install --user pandas --
python3 -m pip install --user homography --
python3 -m pip install --user pymap3d --

apt-get update -y
apt-get upgrade -y
apt-get -y autoclean && apt-get -y clean && apt-get -y autoremove
echo "${NAME} FINISHED"