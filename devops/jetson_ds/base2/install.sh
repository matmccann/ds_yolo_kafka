#!/bin/bash

# Build script
# Example usage: sudo -H ./install.sh
set -eE -o functrace
export NAME="[install.sh] "
failure() {
  local lineno=$1
  local msg=$2
  echo "${NAME} Failed at $lineno: $msg"
}
trap '${NAME} failure ${LINENO} "$BASH_COMMAND"' ERR

echo "${NAME} Starting installations"
/jetson_mist/mist/mist_deps.sh --

echo "${NAME} Finished"
