#!/bin/bash

# Build deepstream gst libraries
# Example usage: sudo -H ./build_gst_libraries.sh
set -eE -o functrace
NAME="[build_gst_libraries] "
failure() {
  local lineno=$1
  local msg=$2
  echo "${NAME} Failed at $lineno: $msg"
}
trap '${NAME} failure ${LINENO} "$BASH_COMMAND"' ERR

# Custom fix to detector confidence bug: detector confidence was set to -0.1 and not passed into gstreamer buffer
# example usage: sudo -H ./build_gst_libraries.sh "11.1"
CUDA_VER=$1

echo "${NAME}: Starting script"
echo "${NAME}: STEP 1: Validate cuda version (${CUDA_VER})"
CUDA_INSTALLED="$(/usr/local/cuda/bin/nvcc --version | grep release | cut -d' ' -f 5 | cut -d',' -f1)"
if [[ "$CUDA_INSTALLED" == "$CUDA_VER" ]]; then
  echo "--(pass)--Correct Cuda version installed: ${CUDA_VER}"
else
  echo "--(fail)--Incorrect Cuda version installed (${CUDA_INSTALLED}) when expects (${CUDA_VER})"
  exit 1
fi

echo "${NAME} Setup kafka protocol adapters (nvidia's kafka_protocol_adaptor)"
make -C /opt/nvidia/deepstream/deepstream/sources/libs/kafka_protocol_adaptor;

echo "${NAME} Build nvdsinfer with CUDA_VER=(${CUDA_INSTALLED})"
cd /opt/nvidia/deepstream/deepstream/sources/libs/nvdsinfer;
make CUDA_VER="${CUDA_INSTALLED}" --;
make install CUDA_VER="${CUDA_INSTALLED}" --;

echo "${NAME} Recompile parser libraries"
#make -C /opt/nvidia/deepstream/deepstream/sources/libs/nvdsinfer_customparser
cd /opt/nvidia/deepstream/deepstream/sources/libs/nvmsgconv;
make --;
make install --;

#echo "${NAME} logging setup to /tmp/nvds/ds.log (go to logger /opt/nvidia/deepstream/deepstream/sources/tools/nvds_logger/README for more info)"
#chmod u+x /opt/nvidia/deepstream/deepstream/sources/tools/nvds_logger/setup_nvds_logger.sh
#/opt/nvidia/deepstream/deepstream/sources/tools/nvds_logger/setup_nvds_logger.sh --

echo "${NAME} Build python binding"

apt install -y git python-dev python3 python3-pip python3.6-dev python3.8-dev cmake g++ build-essential \
    libglib2.0-dev libglib2.0-dev-bin python-gi-dev libtool m4 autoconf automake

DL="$(test -d /opt/nvidia/deepstream/deepstream/sources/deepstream_python_apps && echo 'yes' || echo 'no')"
if [[ "$DL" == "yes" ]]; then
  echo "--(pass)-- Dir exists: /opt/nvidia/deepstream/deepstream/sources/deepstream_python_apps "
else
  cd /opt/nvidia/deepstream/deepstream/sources; git clone https://github.com/NVIDIA-AI-IOT/deepstream_python_apps.git --
  DL="$(test -d /opt/nvidia/deepstream/deepstream/sources/deepstream_python_apps && echo 'yes' || echo 'no')"
  if [[ "$DL" != "yes" ]]; then
    echo "--(fail)-- git clone https://github.com/NVIDIA-AI-IOT/deepstream_python_apps.git"
    exit 1
  fi
fi

# if jetpack 4.5, else jetpack 4.6
if [[]]; then
  echo "${NAME} Detected JP4.6 deepstream 6.xx "
  cd /opt/nvidia/deepstream/deepstream/sources/deepstream_python_apps;
  git submodule update --init;
  cd deepstream_python_apps/bindings
  mkdir build
  cd build
  cmake .. -DPYTHON_MAJOR_VERSION=3 -DPYTHON_MINOR_VERSION=6 -DPIP_PLATFORM=linux_aarch64 -DDS_PATH=/opt/nvidia/deepstream/deepstream/
  make
else
  echo "${NAME} Detected JP4.5 deepstream 5.xx "
  cd /opt/nvidia/deepstream/deepstream/lib;
  python3 setup.py install --

echo "${NAME}: Finished"
