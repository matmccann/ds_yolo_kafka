#!/bin/bash

# Build script
# Example usage: sudo -H ./install.sh CUDA_VER="11.1"
set -eE -o functrace
export NAME="[install.sh] "
failure() {
  local lineno=$1
  local msg=$2
  echo "${NAME} Failed at $lineno: $msg"
}
trap '${NAME} failure ${LINENO} "$BASH_COMMAND"' ERR

export CUDA_VER=$1
echo "${NAME} Starting installation scripts for jetson base"

/jetson_mist/base/ds_deps.sh $CUDA_VER --
/jetson_mist/base/build_gst_libraries.sh $CUDA_VER --

apt-get -y upgrade
apt-get -y update
apt-get -y autoclean
apt-get -y autoremove
apt-get -y clean
rm -rf /jetson_mist/base --
echo "${NAME} Finished."
