#!/bin/bash

# Set up deepstream dependencies
# EXAMPLE USAGE: sudo -H ./ds_deps.sh "11.1"
set -eE -o functrace
export NAME="[ds_deps.sh]: "
failure() {
  local lineno=$1
  local msg=$2
  echo "${NAME} Failed at $lineno: $msg"
}
trap '${NAME} failure ${LINENO} "$BASH_COMMAND"' ERR

# Forces installation to fail if this version of CUDA is not on the computer
export CUDA_VER=$1
export WORKDIR=/jetson_mist/base
mkdir -p $WORKDIR

echo "${NAME} STARTING SCRIPT"
echo "Jetson Setup for fresh install using cuda version ${CUDA_VER}."
echo "For more information about cuda version: nvcc --version "

echo "${NAME} STEP 1: Validate CUDA Version is ${CUDA_VER}"
CUDA_INSTALLED="$(/usr/local/cuda/bin/nvcc --version | grep release | cut -d' ' -f 5 | cut -d',' -f1)"

if [[ "$CUDA_INSTALLED" == "$CUDA_VER" ]]; then
  echo "--(pass)-- Cuda version is ${CUDA_VER} as expected"
else
  echo "--(fail)-- Incorrect cuda version installed. Check your version: /usr/local/cuda/bin/nvcc --version"
  exit 1
fi


echo "${NAME} Basic gstreamer and installations"
apt-get clean && apt-get update -y
#sudo apt-get -y autoclean && sudo apt-get -y clean && sudo apt-get -y autoremove

echo "${NAME} Installing gstreamer libraries"

apt-get install -y \
    build-essential graphviz tzdata python3-pip git pkg-config apt-utils curl \
    libssl1.0.0 \
    libgstreamer1.0-dev libgstreamer-plugins-base1.0-dev libgstreamer-plugins-bad1.0-dev \
    gstreamer1.0-plugins-base \
    gstreamer1.0-doc \
    gstreamer1.0-x \
    gstreamer1.0-gl \
    gstreamer1.0-gtk3 \
    gstreamer1.0-qt5 \
    gstreamer1.0-pulseaudio \
    gstreamer1.0-tools \
    gstreamer1.0-plugins-good \
    gstreamer1.0-plugins-bad \
    gstreamer1.0-plugins-ugly \
    gstreamer1.0-libav \
    gstreamer1.0-alsa \
    libgstrtspserver-1.0-0 \
    libopencv-dev \
    libglib2.0-0 libglib2.0-bin libglib2.0-dev-bin \
    libglib2.0-dev libjson-glib-dev libglib2.0 \
    uuid-dev \
    libjansson4  libjansson-dev libssl-dev json-glib-1.0 \
    libhdf5-dev graphviz \
    libgirepository1.0-dev \
    libcairo2-dev python3-dev \
    python-gi-dev python3-gi python3-gst-1.0

echo "${NAME} updating pip"
python3 -m pip install --user --upgrade wheel pip setuptools --
echo "${NAME} installing pycairo"
python3 -m pip install --user pycairo --
echo "${NAME} installing gstreamer-python"
python3 -m pip install --user git+https://github.com/jackersson/gstreamer-python.git --

echo "${NAME} setup kafka protocol adapters (librdkafka install)"
echo "--(test)-- directory exits? : ${WORKDIR}/kafka_libs"
export KAFKA_COPY='/opt/nvidia/deepstream/deepstream/lib/'

DL="$(test -d $WORKDIR/kafka_libs && echo 'yes' || echo 'no')"
if [[ "$DL" == "no" ]]; then
  echo "--(test)-- directory exits? : ${WORKDIR}/librdkafka"
  DL="$(test -d $WORKDIR/librdkafka && echo 'yes' || echo 'no')"
  if [[ "$DL" == "no" ]]; then
    echo "--(pass)-- doesn't exist ... running git clone https://github.com/edenhill/librdkafka.git"
    cd $WORKDIR; git clone https://github.com/edenhill/librdkafka.git;
    cd $WORKDIR/librdkafka;
    git reset --hard 7101c2310341ab3f4675fc565f64f0967e135a6a;
    ./configure --enable-ssl;
    make --;
    make install --;
  else
    echo "--(pass)-- exists! (${WORKDIR}/librdkafka). try: cp /usr/local/lib/librdkafka* ${KAFKA_COPY}"
    cp /usr/local/lib/librdkafka* "${KAFKA_COPY}" --;
  fi
else
  echo "--(pass)-- exists (${WORKDIR}/kafka_libs). copy to ${KAFKA_COPY}"
  cp -a "${WORKDIR}/kafka_libs/." "${KAFKA_COPY}" --;
fi

ldconfig --

echo "${NAME} FINISHED"
